#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/stat.h>

#define AUTHENTICATION_QUEUE_KEY 12345
#define TRANSFER_QUEUE_KEY 54321
#define CREDS_QUEUE_KEY 98765

// Structure for message content
struct Message {
    long mtype;
    char mtext[256];
};

int main() {
    // Connect to the message queue for authentication
    int creds_queue_id = msgget(CREDS_QUEUE_KEY, 0666);

    if (creds_queue_id == -1) {
        perror("Failed to connect to the authentication message queue");
        exit(1);
    }

    int auth_queue_id = msgget(AUTHENTICATION_QUEUE_KEY, 0666);
    int transfer_queue_id = msgget(TRANSFER_QUEUE_KEY, 0666);

    if (auth_queue_id == -1 || transfer_queue_id == -1) {
        perror("Failed to connect to the message queues");
        exit(1);
    }

    int authenticated = 0;  // Variable to track authentication status
    int creds_sent = 0;

    // Menggunakan system dan wget untuk mengunduh folder "users" dari tautan
    char wgetCommand[256];
    sprintf(wgetCommand, "wget --quiet --no-check-certificate 'https://drive.google.com/uc?id=1CrERpikZwuxgAwDvrhRnB2VyAYtT2SIf&export=download' -O users.zip");
    system(wgetCommand);

    // Menggunakan system untuk mengekstrak "users.txt" dari arsip ke direktori "users"
    system("unzip -oj users.zip -d users");
    system("rm -r users.zip");

    while (1) {
        // Read a command from the user
        printf("Enter a command (e.g., AUTH: username password, TRANSFER filename, CREDS, EXIT): ");
        char command[256];
        fgets(command, sizeof(command), stdin);
        command[strcspn(command, "\n")] = '\0'; // Remove the newline character

        // Send the command to the appropriate message queue
        struct Message msg;
        msg.mtype = 1;
        strcpy(msg.mtext, command);

        if (strcmp(command, "CREDS") == 0) {
            if (msgsnd(creds_queue_id, &msg, sizeof(msg.mtext), 0) == -1) {
                perror("Failed to send CREDS command to the message queue");
            } else {
                creds_sent = 1;
                continue;
            }
        } else if (strncmp(command, "AUTH:", 5) == 0) {
            if (creds_sent) {
                if (msgsnd(auth_queue_id, &msg, sizeof(msg.mtext), 0) == -1) {
                    perror("Failed to send authentication command to the message queue");
                } else {
                    // Wait for a response from the server
                    if (msgrcv(auth_queue_id, &msg, sizeof(msg.mtext), 1, 0) != -1) {
                        if (strcmp(msg.mtext, "Authentication successful") == 0) {
                            printf("Server (AUTH): %s\n", msg.mtext);
                            authenticated = 1;  // Set authentication status to success
                        } else {
                            printf("Server (AUTH): %s\n", msg.mtext);
                            exit(1); // Keluar dari program saat autentikasi gagal
                        }
                    } else {
                        perror("Failed to receive response from the server");
                        exit(1); // Keluar dari program jika ada kesalahan menerima pesan
                    }
                }
            } else {
                printf("You must run CREDS before AUTH.\n");
            }
        } else if (authenticated == 0 && strncmp(command, "TRANSFER", 8) == 0) {
            printf("You must authenticate before transferring a file.\n");
        } else if (authenticated && strncmp(command, "TRANSFER", 8) == 0) {
            // Check if "Sender" and "Receiver" directories exist, if not, create them
            struct stat st = {0};
            if (stat("Sender", &st) == -1) {
                mkdir("Sender", 0700); // Create "Sender" directory with read, write, and execute permissions
            }
            if (stat("Receiver", &st) == -1) {
                mkdir("Receiver", 0700); // Create "Receiver" directory with read, write, and execute permissions
            }

            if (msgsnd(transfer_queue_id, &msg, sizeof(msg.mtext), 0) == -1) {
                perror("Failed to send file transfer command to the message queue");
            } else {
                // Wait for the status message from the server
                if (msgrcv(transfer_queue_id, &msg, sizeof(msg.mtext), 1, 0) != -1) {
                    printf("Server (TRANSFER): %s\n", msg.mtext); // Print the file size status message
                } else {
                    perror("Failed to receive status message from the server");
                }
            }
        } else if (strcmp(command, "EXIT") == 0) {
            break;
        } else {
            printf("UNKNOWN COMMAND.\n");
        }
    }

    return 0;
}

