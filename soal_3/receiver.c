#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <signal.h>
#include <openssl/bio.h>
#include <openssl/evp.h>

#define MAX_MSG_SIZE 256
#define AUTHENTICATION_QUEUE_KEY 12345
#define TRANSFER_QUEUE_KEY 54321
#define CREDS_QUEUE_KEY 98765

struct Message {
    long mtype;
    char mtext[MAX_MSG_SIZE];
    
};

//decrypt a Base64-encoded menggunakan openSSL
char* decryptBase64(const char* base64Input) {
    size_t length = strlen(base64Input);
    int decodedLength = length; //diasumsikan sama

    //mengalokasikan memori untuk decoded text
    char* decodedText = (char*)malloc(decodedLength);
    if (!decodedText) {
        perror("Memory allocation error");
        exit(1);
    }

    if (EVP_DecodeBlock((unsigned char*)decodedText, (const unsigned char*)base64Input, length) == -1) { //menggunakan EVP
        perror("Base64 decoding error");
        free(decodedText);
        exit(1);
    }
    decodedText[length] = '\0';

    return decodedText;
}


bool authenticateUser(const char* username, const char* providedPassword) {
    FILE* file = fopen("users/users.txt", "r"); //masuk ke dir users dan membaca isi users.txt
    if (file) {
        char line[256];

        while (fgets(line, sizeof(line), file)) {
            char* colon_position = strchr(line, ':'); //menentukan posisi : untuk AUTH
            if (colon_position != NULL) {
            //split line untuk proses decrypt pass
                *colon_position = '\0'; 
                char* storedUsername = line;
                char* base64Creds = colon_position + 1;
                base64Creds[strcspn(base64Creds, "\r\n")] = '\0'; 
                char* decryptedPassword = decryptBase64(base64Creds);

                //membandingkan username dan password
                if (strcmp(username, storedUsername) == 0 && strcmp(providedPassword, decryptedPassword) == 0) {
                    free(decryptedPassword);
                    fclose(file);
                    return true; //authentication successful
                }

                free(decryptedPassword);
            }
        }

        fclose(file);
    }

    return false; //authentication failed
}

pid_t receiver_pid = 0; //deklarasi pid untuk terminate program
void authenticationProcess(int auth_queue_id) {
    while (true) {
        struct Message msg;
        msgrcv(auth_queue_id, &msg, sizeof(msg.mtext), 1, 0);

        char* command = strtok(msg.mtext, " ");
        char* username = strtok(NULL, " ");
        char* password = strtok(NULL, " ");

        if (command != NULL && username != NULL && password != NULL && strcmp(command, "AUTH:") == 0) {
 
            bool isAuthenticated = authenticateUser(username, password); //proses autentikasi

            if (isAuthenticated) {
                strcpy(msg.mtext, "Authentication successful");
            } else {
                strcpy(msg.mtext, "Authentication failed");

            }

            msgsnd(auth_queue_id, &msg, sizeof(msg.mtext), 0);
            printf("Server (AUTH): %s\n", msg.mtext);

 kill(receiver_pid, SIGTERM);
 exit(0);
        }
    }
}

void fileTransferProcess(int transfer_queue_id) { //melakukan file transfer
    while (true) {
        struct Message msg;
        msgrcv(transfer_queue_id, &msg, sizeof(msg.mtext), 1, 0);

        char* command = strtok(msg.mtext, " ");
        char* filename = strtok(NULL, " ");

        if (strcmp(command, "TRANSFER") == 0 && filename != NULL) {
            char senderPath[256];
            char receiverPath[256];
            FILE* sourceFile;
            FILE* destFile;

            sprintf(senderPath, "Sender/%s", filename);
            sprintf(receiverPath, "Receiver/%s", filename);

            sourceFile = fopen(senderPath, "rb");
            if (sourceFile) {
                destFile = fopen(receiverPath, "wb");
                if (destFile) {
                    char buffer[1024];
                    size_t bytesRead;
                    size_t totalFileSize = 0; //mengecek ukuran file yang ditransfer

                    while ((bytesRead = fread(buffer, 1, sizeof(buffer), sourceFile)) > 0) {
                        fwrite(buffer, 1, bytesRead, destFile);
                        totalFileSize += bytesRead; //menghitung ukuran file yang ditransfer
                    }
                    fclose(destFile);

                    //status transfer file
                    sprintf(msg.mtext, "File transfer successful. Size: %lu KB", totalFileSize / 1024);
                } else {
                    strcpy(msg.mtext, "File transfer failed");
                }
                fclose(sourceFile);
            } else {
                strcpy(msg.mtext, "File not found");
            }

            msgsnd(transfer_queue_id, &msg, sizeof(msg.mtext), 0);
            printf("Server (TRANSFER): %s\n", msg.mtext);
        }
    }
}


void processCredsCommand(int creds_queue_id) { //mengekseskusi command CREDS
 while (true) {
        struct Message msg;
        msgrcv(creds_queue_id, &msg, sizeof(msg.mtext), 1, 0);

        char* command = strtok(msg.mtext, " ");
        
        if (strncmp(command, "CREDS", 5) == 0) {
    FILE *users_file = fopen("users/users.txt", "r"); //membuka the "users" file

    if (users_file == NULL) {
        perror("fopen");
        exit(1);
    }

    char line[MAX_MSG_SIZE];
    while (fgets(line, sizeof(line), users_file)) {
        char *username = strtok(line, ":");
        char *password = strtok(NULL, ":");
        char decrypted_password[MAX_MSG_SIZE];

        if (password != NULL) {
            password[strcspn(password, "\n")] = '\0'; 
            char *decoded_password = decryptBase64(password);

            printf("Username: %s, Password: %s\n", username, decoded_password);
        }
    }

    fclose(users_file);
}
}
}


int main() {
    //message queue
    int auth_queue_id = msgget(AUTHENTICATION_QUEUE_KEY, IPC_CREAT | 0666);
    int transfer_queue_id = msgget(TRANSFER_QUEUE_KEY, IPC_CREAT | 0666);
    int creds_queue_id = msgget(CREDS_QUEUE_KEY, IPC_CREAT | 0666);

    if (auth_queue_id == -1 || transfer_queue_id == -1) {
        perror("Failed to create message queues");
        exit(1);
    }
    
   
    //fork proses untuk 3 command
    pid_t auth_pid = fork();
    if (auth_pid == -1) {
        perror("Failed to fork authentication process");
        exit(1);
    } else if (auth_pid == 0) {
        // This is the child process for authentication
        authenticationProcess(auth_queue_id);
        exit(0);
    }

    pid_t transfer_pid = fork();
    if (transfer_pid == -1) {
        perror("Failed to fork file transfer process");
        exit(1);
    } else if (transfer_pid == 0) {
        fileTransferProcess(transfer_queue_id);
        exit(0);
    }
       pid_t creds_pid = fork();
    if (creds_pid == -1) {
        perror("Failed to fork file CREDS process");
        exit(1);
    } else if (creds_pid == 0) {
        processCredsCommand(creds_queue_id);
        exit(0);
    }
    
 
    waitpid(auth_pid, NULL, 0);
    waitpid(transfer_pid, NULL, 0);

    //menutup message queue
    msgctl(auth_queue_id, IPC_RMID, NULL);
    msgctl(transfer_queue_id, IPC_RMID, NULL);

    return 0;
}
