#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define ROWS1 9
#define COLS2 9

unsigned long long factorial(int n) {
    unsigned long long result = 1;
    for (int i = 1; i <= n; i++) {
        result *= i;
    }
    return result;
}

struct ThreadInfo {
    int value;
    int row;
    int col;
    unsigned long long factorialResult;
};

void *calculateFactorial(void *args) {
    struct ThreadInfo *info = (struct ThreadInfo *)args;
    int value = info->value;
    info->factorialResult = factorial(value);

    return NULL;
}

int main() {
    key_t key = 5678;
    int shmid;
    int (*result)[COLS2];

    shmid = shmget(key, sizeof(int[ROWS1][COLS2]), 0666);
    if (shmid == -1) {
        perror("shmget");
        return 1;
    }

    result = shmat(shmid, NULL, 0);
    if (result == (int (*)[]) -1) {
        perror("shmat");
        return 1;
    }

    printf("ini matriks dari shared memory:\n");
    for(int i = 0; i < ROWS1; i++) {
        for(int j = 0; j < COLS2; j++) {
            printf("%d\t", result[i][j]);
        }
        printf("\n");
    }

    int transpose[COLS2][ROWS1];
    for(int i = 0; i < COLS2; i++) {
        for(int j = 0; j < ROWS1; j++) {
            transpose[i][j] = result[j][i];
        }
    }

    printf("\nini transpose nya:\n");
    for(int i = 0; i < COLS2; i++) {
        for(int j = 0; j < ROWS1; j++) {
            printf("%d\t", transpose[i][j]);
        }
        printf("\n");
    }

    pthread_t t_id[COLS2][ROWS1];
    struct ThreadInfo info[COLS2][ROWS1];

    for (int i = 0; i < COLS2; i++) {
        for (int j = 0; j < ROWS1; j++) {
            info[i][j].value = transpose[i][j];
            info[i][j].row = i;
            info[i][j].col = j;
            pthread_create(&t_id[i][j], NULL, &calculateFactorial, (void *)&info[i][j]);
        }
    }

    for (int i = 0; i < COLS2; i++) {
        for (int j = 0; j < ROWS1; j++) {
            pthread_join(t_id[i][j], NULL);
        }
    }

    printf("\nini faktorial dari transpose nya:\n");
    for (int i = 0; i < COLS2; i++) {
        for (int j = 0; j < ROWS1; j++) {
            printf("%llu\t", info[i][j].factorialResult);
        }
        printf("\n");
    }

    shmdt(result);

    return 0;
}

