#include <stdio.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define ROWS1 9
#define COLS2 9

unsigned long long factorial(int n) {
    unsigned long long result = 1;
    for (int i = 1; i <= n; i++) {
        result *= i;
    }
    return result;
}

int main() {
    key_t key = 5678;
    int shmid;
    int (*result)[COLS2];

    shmid = shmget(key, sizeof(int[ROWS1][COLS2]), 0666);
    if (shmid == -1) {
        perror("shmget");
        return 1;
    }

    result = shmat(shmid, NULL, 0);
    if (result == (int (*)[]) -1) {
        perror("shmat");
        return 1;
    }

    printf("ini hasil matriks shared memory:\n");
    for(int i = 0; i < ROWS1; i++) {
        for(int j = 0; j < COLS2; j++) {
            printf("%d\t", result[i][j]);
        }
        printf("\n");
    }

    int transpose[COLS2][ROWS1];
    for(int i = 0; i < COLS2; i++) {
        for(int j = 0; j < ROWS1; j++) {
            transpose[i][j] = result[j][i];
        }
    }

    printf("\nini Transpose nya:\n");
    for(int i = 0; i < COLS2; i++) {
        for(int j = 0; j < ROWS1; j++) {
            printf("%d\t", transpose[i][j]);
        }
        printf("\n");
    }

    printf("\nini hasil faktorial dari transpose nya:\n");
    for (int i = 0; i < COLS2; i++) {
        for (int j = 0; j < ROWS1; j++) {
            unsigned long long result = factorial(transpose[i][j]);
            printf("%llu\t", result);
        }
        printf("\n");
    }

    shmdt(result);

    return 0;
}

