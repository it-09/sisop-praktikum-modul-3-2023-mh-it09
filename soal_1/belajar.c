#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define ROWS1 9
#define COLS1 2
#define ROWS2 2
#define COLS2 9

void fillMatrix(int rows, int cols, int matrix[rows][cols], int min, int max) {
    for(int i = 0; i < rows; i++) {
        for(int j = 0; j < cols; j++) {
            matrix[i][j] = rand() % (max - min + 1) + min;
        }
    }
}

void multiplyMatrices(int mat1[ROWS1][COLS1], int mat2[ROWS2][COLS2], int result[ROWS1][COLS2]) {
    for(int i = 0; i < ROWS1; i++) {
        for(int j = 0; j < COLS2; j++) {
            result[i][j] = 0;
            for(int k = 0; k < COLS1; k++) {
                result[i][j] += mat1[i][k] * mat2[k][j];
            }
             result[i][j]--;
        }
    }
}


void displayMatrix(int rows, int cols, int matrix[rows][cols]) {
    for(int i = 0; i < rows; i++) {
        for(int j = 0; j < cols; j++) {
            printf("%d\t", matrix[i][j]);
        }
        printf("\n");
    }
}

int main() {
    srand(time(NULL));

    int matrix1[ROWS1][COLS1];
    int matrix2[ROWS2][COLS2];
    int result[ROWS1][COLS2];

    fillMatrix(ROWS1, COLS1, matrix1, 2, 2);
    fillMatrix(ROWS2, COLS2, matrix2, 1, 3);

    multiplyMatrices(matrix1, matrix2, result);

    printf("Hasil perkalian matriks:\n");
    displayMatrix(ROWS1, COLS2, result);

    key_t key = 5678;
    int shmid = shmget(key, sizeof(int[ROWS1][COLS2]), IPC_CREAT | 0666);
    int (*sharedResult)[COLS2] = shmat(shmid, NULL, 0);

    for(int i = 0; i < ROWS1; i++) {
        for(int j = 0; j < COLS2; j++) {
            sharedResult[i][j] = result[i][j];
        }
    }

    shmdt(sharedResult);
    return 0;
}
