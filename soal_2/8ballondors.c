#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <ctype.h>

#define MAX_WORD_LENGHT 100
#define MAX_FILENAME_LENGHT 100
#define LOG_FILENAME "frekuensi.log"

void buat_log(const char* type, const char* message) {
    time_t now;
    time(&now);
    char timestamp[20];
    strftime(timestamp, 20, "[%d/%m/%y %H:%M:%S]", localtime(&now));
    FILE* log_file = fopen(LOG_FILENAME, "a");
    if (log_file) {
        fprintf(log_file, "%s [%s] %s\n", timestamp, type, message);
        fclose(log_file);
    }
}

void hapus_nonhuruf() {
    FILE *input_file = fopen("lirik.txt", "r");
    FILE *output_file = fopen("thebeatles.txt", "w");

    if (input_file == NULL || output_file == NULL) {
        perror("Failed to open file");
        exit(1);
    }

    int c;
    while ((c = fgetc(input_file)) != EOF) {
        if (isalpha(c) || c == ' ' || c == '\n') {
            fputc(c, output_file);
        }
    }

    fclose(input_file);
    fclose(output_file);
}

void hitung_kata(const char* word) {
    char filename[MAX_FILENAME_LENGHT];
    sprintf(filename, "thebeatles.txt");

    int count = 0;
    char word_to_search[MAX_WORD_LENGHT];
    sprintf(word_to_search, " %s ", word);

    FILE *input_file = fopen(filename, "r");
    if (input_file == NULL) {
        perror("Failed to open file");
        exit(1);
    }

    int in_word = 0;
    char current_word[MAX_WORD_LENGHT] = {0};
    int word_len = 0;

    int c;
    while ((c = fgetc(input_file)) != EOF) {
        if (isalpha(c)) {
            current_word[word_len++] = c;
            in_word = 1;
        } else {
            if (in_word) {
                current_word[word_len] = '\0';
                if (strcmp(current_word, word) == 0) {
                    count++;
                }
                word_len = 0;
                in_word = 0;
            }
        }
    }

    fclose(input_file);

    char message[MAX_WORD_LENGHT * 2];
    snprintf(message, sizeof(message), "Kata '%s' muncul sebanyak %d kali dalam file '%s'", word, count, filename);
    buat_log("KATA", message);
    printf("%s\n", message);
}

void hitung_huruf(const char* letter) {
    char filename[MAX_FILENAME_LENGHT];
    sprintf(filename, "thebeatles.txt");

    int count = 0;

    FILE *input_file = fopen(filename, "r");
    if (input_file == NULL) {
        perror("Failed to open file");
        exit(1);
    }

    char character = letter[0];

    int c;
    while ((c = fgetc(input_file)) != EOF) {
        if (c == character) {
            count++;
        }
    }

    fclose(input_file);

    char message[MAX_WORD_LENGHT * 2];
    snprintf(message, sizeof(message), "Huruf '%c' muncul sebanyak %d kali dalam file '%s'", character, count, filename);
    buat_log("HURUF", message);
    printf("%s\n", message);
}

int main(int argc, char *argv[]) {
    if (argc != 2 || (strcmp(argv[1], "-kata") != 0 && strcmp(argv[1], "-huruf") != 0)) {
        printf("Usage: %s <-kata|-huruf>\n", argv[0]);
        return 1;
    }

    hapus_nonhuruf();

    int pipefd[2];
    if (pipe(pipefd) == -1) {
        perror("Pipe creation failed");
        exit(1);
    }

    pid_t child_pid = fork();

    if (child_pid < 0) {
        perror("Fork failed");
        exit(1);
    }

    if (child_pid == 0) {
        // Proses child
        close(pipefd[0]);

        if (strcmp(argv[1], "-kata") == 0) {
            char word[MAX_WORD_LENGHT];
            printf("Masukkan kata yang ingin dihitung: ");
            scanf("%s", word);
            hitung_kata(word);
        } else if (strcmp(argv[1], "-huruf") == 0) {
            char letter[MAX_WORD_LENGHT];
            printf("Masukkan huruf yang ingin dihitung: ");
            scanf("%s", letter);
            hitung_huruf(letter);
        }

        close(pipefd[1]);
        exit(0);
    } else {
        close(pipefd[1]);
        int status;
        wait(&status);
    }

    return 0;
}
