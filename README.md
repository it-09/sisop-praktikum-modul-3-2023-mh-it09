# **LAPORAN RESMI PRAKTIKUM SISTEM OPERASI MODUL 3**
Berikut adalah Laporan Resmi Praktikum Sistem Operasi Modul 3 oleh Kelompok IT 09.

---
## **TABLE OF CONTENT**

 - [Soal 1](#soal1)
 - [Soal 2](#soal2)
 - [Soal 3](#soal3)
 - [Soal 4](#soal4)
 
---
### **I. SOAL 1<a name="soal1"></a>**
**A. PEMBAHASAN**
1. Soal pertama mengharuskan kita untuk membuat program C dengan nama `belajar.c`, `yang.c`, dan `rajin.c`. Secara singkat, program `belajar.c` akan berisi program untuk melakukan perkalian matriks dengan ukuran 9x2 dan 2x9 ("9" karena sesuai nomor Kelompok kami yaitu 9), matriks nantinya akan berisi angka random, dan hasil dari perkalian tersebut dikurang 1 di setiap matriks. Berikut ini adalah program `belajar.c` yang telah saya buat:

```c
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define ROWS1 9
#define COLS1 2
#define ROWS2 2
#define COLS2 9

void fillMatrix(int rows, int cols, int matrix[rows][cols], int min, int max) {
    for(int i = 0; i < rows; i++) {
        for(int j = 0; j < cols; j++) {
            matrix[i][j] = rand() % (max - min + 1) + min;
        }
    }
}

void multiplyMatrices(int mat1[ROWS1][COLS1], int mat2[ROWS2][COLS2], int result[ROWS1][COLS2]) {
    for(int i = 0; i < ROWS1; i++) {
        for(int j = 0; j < COLS2; j++) {
            result[i][j] = 0;
            for(int k = 0; k < COLS1; k++) {
                result[i][j] += mat1[i][k] * mat2[k][j];
            }
             result[i][j]--;
        }
    }
}


void displayMatrix(int rows, int cols, int matrix[rows][cols]) {
    for(int i = 0; i < rows; i++) {
        for(int j = 0; j < cols; j++) {
            printf("%d\t", matrix[i][j]);
        }
        printf("\n");
    }
}

int main() {
    srand(time(NULL));

    int matrix1[ROWS1][COLS1];
    int matrix2[ROWS2][COLS2];
    int result[ROWS1][COLS2];

    fillMatrix(ROWS1, COLS1, matrix1, 2, 2);
    fillMatrix(ROWS2, COLS2, matrix2, 1, 3);

    multiplyMatrices(matrix1, matrix2, result);

    printf("Hasil perkalian matriks:\n");
    displayMatrix(ROWS1, COLS2, result);

    key_t key = 5678;
    int shmid = shmget(key, sizeof(int[ROWS1][COLS2]), IPC_CREAT | 0666);
    int (*sharedResult)[COLS2] = shmat(shmid, NULL, 0);

    for(int i = 0; i < ROWS1; i++) {
        for(int j = 0; j < COLS2; j++) {
            sharedResult[i][j] = result[i][j];
        }
    }

    shmdt(sharedResult);
    return 0;
}
```
2. Pada `voidfillMatrix`, Ini adalah deklarasi fungsi bernama fillMatrix yang mengambil beberapa argumen. 
`int rows, int cols` adalah parameter yang diterima oleh fungsi, mewakili jumlah baris dan kolom dari matriks yang akan diisi. 
`int matrix[rows][cols]` adalah parameter lainnya, matriks yang akan diisi. Array dua dimensi dengan ukuran yang sesuai. 
`int min, int max` adalah nilai minimal dan maksimal yang akan digunakan untuk mengisi matriks dengan angka acak.
`matrix[i][j] = rand() % (max - min + 1) + min;` - Pada setiap sel dalam matriks (`matrix[i][j]`), kita memberikan nilai acak. 
 - `rand()` menghasilkan angka acak.
 - `rand() % (max - min + 1)` membatasi rentang angka yang dihasilkan antara `min` dan `max`.
 - `+ min` menyesuaikan angka agar sesuai dengan rentang yang diinginkan.

3. Pada `void multiplyMatrices` bertujuan untuk melakukan perkalian matriks. `for(int i = 0; i < ROWS1; i++)` - Loop pertama dimulai dari i = 0 (baris pertama matriks hasil) dan terus berjalan hingga i mencapai jumlah baris matriks pertama (ROWS1). `for(int j = 0; j < COLS2; j++)`- Di setiap baris dari matriks hasil, loop kedua dimulai dari j = 0 (kolom pertama matriks hasil) dan berjalan hingga j mencapai jumlah kolom matriks kedua (COLS2).`result[i][j] = 0;` - Pada setiap sel dalam matriks hasil (`result[i][j]`), kita menginisialisasi nilainya menjadi 0 sebelum melakukan perkalian.`for(int k = 0; k < COLS1; k++) {` - Selanjutnya, kita memiliki loop ketiga yang akan berjalan hingga `k` mencapai jumlah kolom matriks pertama (`COLS1`).`result[i][j] += mat1[i][k] * mat2[k][j];` - Pada setiap iterasi loop ketiga, kita melakukan perkalian antara elemen matriks pertama (`mat1[i][k]`) dan matriks kedua (`mat2[k][j]`). Hasil dari perkalian ini ditambahkan ke nilai sebelumnya di matriks hasil (`result[i][j]`).`result[i][j]--;` - Setelah selesai melakukan perkalian, kita mengurangi 1 dari nilai di matriks hasil. Ini merupakan langkah tambahan yang dilakukan sesuai permintaan Anda dalam soal.Setelah loop kedua selesai, loop pertama akan beralih ke baris berikutnya dan langkah 1.1 akan diulang hingga semua sel dalam matriks hasil terisi dengan hasil perkalian.

4. Fungsi `displayMatrix` bertujuan untuk menampilkan matriks ke konsol atau layar. `void displayMatrix(int rows, int cols, int matrix[rows][cols])`adalah deklarasi fungsi displayMatrix. Fungsi ini akan menerima tiga argumen: rows (jumlah baris matriks), cols (jumlah kolom matriks), dan matrix (matriks yang akan ditampilkan).
`for(int i = 0; i < rows; i++)` Loop pertama dimulai dari i = 0 (baris pertama matriks) dan berjalan hingga i mencapai jumlah baris matriks (rows).`for(int j = 0; j < cols; j++) {` Di setiap baris dari matriks, loop kedua dimulai dari j = 0 (kolom pertama matriks) dan berjalan hingga j mencapai jumlah kolom matriks (cols). `printf("%d\t", matrix[i][j])` Pada setiap sel dalam matriks (matrix[i][j]), kita menggunakan fungsi printf untuk menampilkan nilai elemen matriks ke layar. %d digunakan untuk memformat dan menampilkan nilai integer. \t menambahkan tab untuk memformat tata letak tampilan.`printf("\n")` Setelah loop kedua selesai (yaitu, setelah semua kolom pada baris ini telah ditampilkan), kita menggunakan printf("\n") untuk pindah ke baris baru dalam tampilan. Ini memastikan bahwa setiap baris dari matriks ditampilkan secara terpisah.

5. `srand(time(NULL));` - Inisialisasi generator angka acak. Ini memastikan bahwa setiap kali program dijalankan, angka acak yang dihasilkan akan berbeda. `int matrix1[ROWS1][COLS1];` - Matriks pertama dengan ukuran 9x2. `int matrix2[ROWS2][COLS2];` - Matriks kedua dengan ukuran 2x9. `int result[ROWS1][COLS2];` - Matriks untuk menyimpan hasil perkalian. Mengisi matriks pertama dan kedua dengan angka acak sesuai dengan rentang yang diberikan. Memanggil fungsi `multiplyMatrices` untuk melakukan perkalian matriks dan menyimpan hasilnya di matriks result. Menampilkan hasil perkalian matriks ke layar menggunakan fungsi `displayMatrix`.Membuat atau mengakses segmen shared memory dengan kunci '5678'. Mengaitkan segmen shared memory dengan program dan mendapatkan pointer ke matriks dengan `shmat`. Menyalin hasil perkalian matriks ke shared memory.Melepaskan koneksi dengan shared memory menggunakan shmdt. Secara keseluruhan bertujuan untuk melakukan perkalian matriks, menampilkan hasilnya, dan menyimpan hasil perkalian di shared memory untuk digunakan oleh program lain.

6. Lalu pada program `yang.c` mengharuskan kita untuk menerapkan konsep `shared memory`, konsep `thread atau multithreading` dan juga melakukan transpose matriks. Berikut ini adalah program `yang.c yang telah saya buat:

```c
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define ROWS1 9
#define COLS2 9

unsigned long long factorial(int n) {
    unsigned long long result = 1;
    for (int i = 1; i <= n; i++) {
        result *= i;
    }
    return result;
}

struct ThreadInfo {
    int value;
    int row;
    int col;
    unsigned long long factorialResult;
};

void *calculateFactorial(void *args) {
    struct ThreadInfo *info = (struct ThreadInfo *)args;
    int value = info->value;
    info->factorialResult = factorial(value);

    return NULL;
}

int main() {
    key_t key = 5678;
    int shmid;
    int (*result)[COLS2];

    shmid = shmget(key, sizeof(int[ROWS1][COLS2]), 0666);
    if (shmid == -1) {
        perror("shmget");
        return 1;
    }

    result = shmat(shmid, NULL, 0);
    if (result == (int (*)[]) -1) {
        perror("shmat");
        return 1;
    }

    printf("ini matriks dari shared memory:\n");
    for(int i = 0; i < ROWS1; i++) {
        for(int j = 0; j < COLS2; j++) {
            printf("%d\t", result[i][j]);
        }
        printf("\n");
    }

    int transpose[COLS2][ROWS1];
    for(int i = 0; i < COLS2; i++) {
        for(int j = 0; j < ROWS1; j++) {
            transpose[i][j] = result[j][i];
        }
    }

    printf("\nini transpose nya:\n");
    for(int i = 0; i < COLS2; i++) {
        for(int j = 0; j < ROWS1; j++) {
            printf("%d\t", transpose[i][j]);
        }
        printf("\n");
    }

    pthread_t t_id[COLS2][ROWS1];
    struct ThreadInfo info[COLS2][ROWS1];

    for (int i = 0; i < COLS2; i++) {
        for (int j = 0; j < ROWS1; j++) {
            info[i][j].value = transpose[i][j];
            info[i][j].row = i;
            info[i][j].col = j;
            pthread_create(&t_id[i][j], NULL, &calculateFactorial, (void *)&info[i][j]);
        }
    }

    for (int i = 0; i < COLS2; i++) {
        for (int j = 0; j < ROWS1; j++) {
            pthread_join(t_id[i][j], NULL);
        }
    }

    printf("\nini faktorial dari transpose nya:\n");
    for (int i = 0; i < COLS2; i++) {
        for (int j = 0; j < ROWS1; j++) {
            printf("%llu\t", info[i][j].factorialResult);
        }
        printf("\n");
    }

    shmdt(result);

    return 0;
}
```

7. `unsigned long long factorial(int n) {` adalah sebuah fungsi untuk menghitung faktorial dari suatu angka.  Ia mengalikan semua bilangan bulat positif dari 1 hingga n untuk mendapatkan hasil faktorialnya.

8. `struct ThreadInfo {` mendefinisikan struktur ThreadInfo yang memiliki empat anggota. value: Menyimpan nilai dari matriks transpose.
row: Menyimpan indeks baris dari matriks transpose.
col: Menyimpan indeks kolom dari matriks transpose.
factorialResult: Menyimpan hasil faktorial dari value.

9. `void *calculateFactorial(void *args) {` adalah fungsi yang akan dijalankan oleh setiap thread untuk menghitung faktorial dari suatu angka. Secara keseluruhan, fungsi calculateFactorial ini menerima argumen berupa struktur ThreadInfo, mengambil nilai value dari struktur tersebut, menghitung faktorialnya, dan menyimpan hasilnya kembali dalam struktur ThreadInfo.

10. Pada fungsi `main` nya, pertama-tama kita mendeklarasikan variabel key sebagai tipe data key_t. key_t adalah tipe data untuk kunci (key) yang digunakan dalam alokasi shared memory. Di sini, kita inisialisasi key dengan nilai 5678.Selanjutnya, kita mendeklarasikan variabel shmid yang akan digunakan untuk menyimpan ID dari segmen shared memory. shmid adalah integer yang akan digunakan untuk mengidentifikasi segmen shared memory. `int (*result)[COLS2];` adalah deklarasi variabel result sebagai pointer ke array dua dimensi dengan jumlah kolom sebanyak COLS2. result akan digunakan untuk menunjuk ke segmen shared memory. Kita menggunakan shmget untuk mendapatkan akses ke segmen shared memory. shmget membutuhkan tiga argumen: key (kunci unik untuk identifikasi segmen), ukuran dari segmen shared memory yang akan dialokasikan (dalam byte), dan hak akses (mode). Mode 0666 memberikan hak akses read-write untuk semua pengguna. Jika shmget mengembalikan -1, itu berarti ada kesalahan. Pada kasus ini, kita menggunakan perror untuk mencetak pesan kesalahan dan kemudian mengembalikan 1 untuk menandakan bahwa program berakhir dengan kesalahan. Setelah berhasil mendapatkan ID segmen shared memory (shmid), kita mengaitkan segmen tersebut dengan ruang alamat proses menggunakan shmat. shmat mengembalikan alamat dari segmen shared memory yang berhasil di-attach. Jika shmat mengembalikan `(int (*)[]) -1`, itu berarti ada kesalahan dalam mengaitkan segmen shared memory. Sama seperti sebelumnya, kita mencetak pesan kesalahan menggunakan perror dan mengembalikan 1.

11. `printf("ini matriks dari shared memory:\n");
` potongan kode ini bertanggung jawab untuk menampilkan matriks yang disimpan di dalam shared memory ke konsol.

12. `int transpose[COLS2][ROWS1];` Deklarasi ini membuat matriks baru dengan nama transpose yang memiliki jumlah baris sebanyak COLS2 (kolom matriks awal) dan jumlah kolom sebanyak ROWS1 (baris matriks awal). Ini adalah matriks hasil dari operasi transpose. Loop pertama `(for(int i = 0; i < COLS2; i++))` digunakan untuk mengakses setiap kolom dalam matriks asli. Di dalam loop pertama, ada loop kedua `(`for(int j = 0; j < ROWS1; j++))` yang digunakan untuk mengakses setiap baris dalam matriks asli. Pada setiap iterasi, nilai dari result[j][i] (elemen matriks awal pada baris ke-j dan kolom ke-i) disalin ke transpose[i][j] (elemen matriks transpose pada baris ke-i dan kolom ke-j). Secara keseluruhan, kode ini melakukan iterasi pada matriks awal dan menukar baris dengan kolom untuk membuat matriks transpose baru.

13. `printf("\nini transpose nya:\n");` kurang lebih sama seperti sebelumnya, secara singkat ia menampilkan matriks transpose ke konosl.

14. `pthread_t t_id[COLS2][ROWS1];` Ini adalah deklarasi dari array dua dimensi t_id yang bertipe pthread_t. Array ini akan digunakan untuk menyimpan ID dari setiap thread yang akan dibuat. `struct ThreadInfo info[COLS2][ROWS1];` Ini adalah deklarasi dari array dua dimensi info yang bertipe struct ThreadInfo. Setiap elemen dari array info akan menyimpan informasi yang diperlukan untuk perhitungan faktorial di dalam thread. Terdapat dua loop bersarang (for) untuk mengakses setiap elemen dalam matriks transpose. Di dalam loop, setiap elemen dari info diisi dengan nilai dari matriks transpose dan posisi baris dan kolomnya. Lalu, sebuah thread baru dibuat menggunakan pthread_create. Thread ini akan menjalankan fungsi calculateFactorial dengan argumen berupa alamat dari info[i][j].
Selanjutnya, terdapat dua loop bersarang lagi untuk menunggu setiap thread selesai. Di dalam loop, pthread_join digunakan untuk menunggu thread dengan ID t_id[i][j] selesai. Artinya, program akan menunggu hingga thread tersebut selesai sebelum melanjutkan eksekusi. Setelah semua thread selesai, hasil faktorial dari setiap elemen akan dicetak.

15. Dan pada program yang terakhir yaitu `rajin.c` berisi kurang lebih sama seperti program `yang.c` namun perbedaannya terletak pada penggunaan thread atau multithreading nya. Program `rajin.c` tidak perlu menggunakan thread atau multithreading untuk mengetahui bagaimana perbedaan ketika sebuah program menggunakan thread atau tidak. Berikut ini adalah program `rajin.c` yang telah saya buat:

```c
#include <stdio.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define ROWS1 9
#define COLS2 9

unsigned long long factorial(int n) {
    unsigned long long result = 1;
    for (int i = 1; i <= n; i++) {
        result *= i;
    }
    return result;
}

int main() {
    key_t key = 5678;
    int shmid;
    int (*result)[COLS2];

    shmid = shmget(key, sizeof(int[ROWS1][COLS2]), 0666);
    if (shmid == -1) {
        perror("shmget");
        return 1;
    }

    result = shmat(shmid, NULL, 0);
    if (result == (int (*)[]) -1) {
        perror("shmat");
        return 1;
    }

    printf("ini hasil matriks shared memory:\n");
    for(int i = 0; i < ROWS1; i++) {
        for(int j = 0; j < COLS2; j++) {
            printf("%d\t", result[i][j]);
        }
        printf("\n");
    }

    int transpose[COLS2][ROWS1];
    for(int i = 0; i < COLS2; i++) {
        for(int j = 0; j < ROWS1; j++) {
            transpose[i][j] = result[j][i];
        }
    }

    printf("\nini Transpose nya:\n");
    for(int i = 0; i < COLS2; i++) {
        for(int j = 0; j < ROWS1; j++) {
            printf("%d\t", transpose[i][j]);
        }
        printf("\n");
    }

    printf("\nini hasil faktorial dari transpose nya:\n");
    for (int i = 0; i < COLS2; i++) {
        for (int j = 0; j < ROWS1; j++) {
            unsigned long long result = factorial(transpose[i][j]);
            printf("%llu\t", result);
        }
        printf("\n");
    }

    shmdt(result);

    return 0;
}
```

16. Secara keseluruhan, struktur kode pada program `rajin.c` memiliki kemiripan pada `yang.c`.

**B. HASIL**
1. Ini adalah hasil atau output dari `belajar.c`

![1_belajar_c](/uploads/c7688c2199b3a68b032073a6759893bf/1_belajar_c.PNG)


2. Ini adalah hasil atau output dari `yang.c`

![2_yang_c](/uploads/2a45b10a8fef1fc4637ebd3b0e030219/2_yang_c.PNG)


3. Ini adalah hasil atau output dari `rajin.c`

![3_rajin_c](/uploads/6553ebfb4d5244ec6bec601306f2021f/3_rajin_c.PNG)


**C. REVISI**
---
Tidak ada.

### **II. SOAL 2<a name="soal2"></a>**
**A. PEMBAHASAN**
1. Hal pertama yang harus dilakukan untuk mengerjakan soal ini adalah mendownload lirik.txt dari link yang sudah diberikan menggunakan `wget -O lirik.txt https://drive.google.com/file/d/16FQ11ynf1hSpREPU1jlmzj6xvNl8oosW/view?usp=sharing` pada directory yang diinginkan. Setelah didownload, file lirik.txt akan muncul dan memiliki isi lirik sebagai berikut :

```
!!Sudah kudaki gunung tertinggi, hanya untuk mencari di mana dirimu. Sudah kujelajahi isi bumi, hanya untuk dapat hidup bersamamu. Sudah kuarungi laut samudera, hanya untuk mencari tempat berlabuhmu. Tapi semakin jauh ku mencari cinta, semakin aku tak mengerti. Akulah Arjuna!! yang mencari cinta. Wahai wanita, cintailah aku. Mungkin kutemui cinta sejati saat aku hembuskan nafas terakhirku. Mungkin cinta sejati memang tak ada dalam cerita kehidupan ini. Akulah Arjuna!!! yang mencari cinta. Wahai wanita, cintailah aku. Akulah Arjuna!! yang mencari cinta. Wahai wanita, cintailah aku. Akulah Arjuna!! yang mencari cinta. Wahai wanita!!, cintailah aku!!. Usap air matamu (yang menetes di pipimu), ku pastikan semuanya akan baik-baik saja bila kau terus pandangi langit tinggi di angkasa. Tak kan ada habisnya segala hasrat di dunia, hawa tercipta di dunia (untuk menemani sang Adam), begitu juga dirimu (tercipta tuk temani aku). Renungkan sejenak arti hadirku di sini, jangan pernah ingkari, dirimu adalah wanita. Harusnya dirimu menjadi perhiasan sangkar maduku, walaupun kadang diriku bertekuk lutut di hadapanmu. Hawa tercipta di dunia (untuk menemani sang Adam), begitu juga dirimu (tercipta tuk temani aku). Harusnya dirimu menjadi perhiasan sangkar maduku, walaupun kadang diriku bertekuk lutut di hadapanmu. Hawa tercipta di dunia (untuk menemani sang Adam), begitu juga dirimu (tercipta tuk temani aku). Bukalah pintu jiwamu, dengar bisikan sanubari. Semua adalah isyarat, isyarat dari Sang Pencipta. Di saat terlihat bintang-bintang atau malam yang gelap gulita, pada saat semangatmu hilang carilah orang lain dan bicara. Manusia tidaklah pernah ditinggal sendirian saja. Terkadang kita tak menyadari hal baik disekitar kita. Hidup bagaikan pesawat kertas, terbang dan membawa cinta kita semua. Sayap yang terbentang dengan percaya diri, dilihat semua orang walau tak tahu cara melipatnya. Suatu saat pasti akan berhasil lalu terbang. Kekuatan harapan yang menerbangkannya. Ya... mari nikmatilah. Sanbyaku rokujugo nichi.Hidup bagaikan pesawat kertas, terbang dan pergi membawa impian. Sekuat tenaga dengan hembusan angin, terus melaju terbang. Jangan bandingkan jarak terbangnya, tapi bagaimana dan apa yang dilalui, karena itulah satu hal yang penting. Selalu sesuai kata hati. Sanbyaku rokujugo nichi. Ayo terbanglah!! (Coba terbanglah!!), ayo terbanglah!! (Coba terbanglah!!), ayo terbanglah!! (Coba terbanglah!!).!!
```

2. Selanjutnya kita buat program 8ballondors.c dengan menerapkan konsep pipes dan fork dengan 2 pipes di dalamnya.

3. Pertama, kita mulai buat kodenya dengan beberapa perintah `#include`, yang mengimpor pustaka standar C yang akan digunakan dalam program. Ini termasuk `stdio.h` untuk fungsi input/output standar, `stdlib.h` untuk alokasi memori, `string.h` untuk pengoperasian string, `time.h` untuk waktu dan tanggal, `unistd.h` untuk fungsi sistem Unix, dan `sys/types.h` dan `sys/wait.h` untuk manajemen proses. Terdapat juga pustaka `ctype.h` untuk operasi pada karakter.

```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <ctype.h>
```

4. Kemudian, kita membuat beberapa definisi menggunakan `#define`, seperti `MAX_WORD_LENGTH` (maksimum panjang kata) sejumlah 100 karakter, `MAX_FILENAME_LENGTH` (maksimum panjang nama file) sejumlah 100 karakter juga, dan `LOG_FILENAME` (nama file log yang akan digunakan) dalam hal ini dalaha `frekuensi.log`.

```c
#define MAX_WORD_LENGTH 100
#define MAX_FILENAME_LENGTH 100
#define LOG_FILENAME "frekuensi.log"
```

5. Lalu kita mulai membuat beberapa fungsi rekursif. Di sini kita menggunakan algoritma fungsi rekursif agar memudahkan pembacaan code dan pemahaman alur berjalannya fungsi. Pertama kita membuat `buat_log`. Fungsi ini untuk mencatat pesan log dalam file `frekuensi.log`. Fungsi ini akan mendapatkan waktu saat ini menggunakan `time()`. Waktu ini kemudian diformat menjadi format yang sesuai dengan format pada soal, seperti "[dd/mm/yy HH:MM:SS]", menggunakan `strftime()`. Setelah itu, fungsi membuka file `frekuensi.log` untuk ditambahkan (mode "a") dan mencetak pesan log beserta tipe dan timestamp ke dalam file tersebut. Terakhir, fungsi menutup file log.

```c
void buat_log(const char* type, const char* message) {
    time_t now;
    time(&now);
    char timestamp[20];
    strftime(timestamp, 20, "[%d/%m/%y %H:%M:%S]", localtime(&now));
    FILE* log_file = fopen(LOG_FILENAME, "a");
    if (log_file) {
        fprintf(log_file, "%s [%s] %s\n", timestamp, type, message);
        fclose(log_file);
    }
}
```

6. Fungsi selanjutnya yang dibuat adalah fungsi `hapus_nonhuruf`. Fungsi `hapus_nonhuruf` memiliki tugas untuk membersihkan teks dalam file "lirik.txt" dari karakter-karakter yang bukan huruf (alphabet), seperti tanda baca. Pertama, fungsi membuka file "lirik.txt" dalam mode baca ("r") dan file "thebeatles.txt" dalam mode tulis ("w"). Selanjutnya, fungsi membaca karakter demi karakter dari "lirik.txt". Setiap karakter yang valid (huruf, spasi, atau newline) disalin ke file "thebeatles.txt". Setelah selesai membaca dan menyalin teks, file "thebeatles.txt" ditutup.

```c
void hapus_nonhuruf() {
    FILE *input_file = fopen("lirik.txt", "r");
    FILE *output_file = fopen("thebeatles.txt", "w");

    if (input_file == NULL || output_file == NULL) {
        perror("Failed to open file");
        exit(1);
    }

    int c;
    while ((c = fgetc(input_file)) != EOF) {
        if (isalpha(c) || c == ' ' || c == '\n') {
            fputc(c, output_file);
        }
    }

    fclose(input_file);
    fclose(output_file);
}
```

7. Setelah itu, kita membuat fungi `hitung_kata`. Fungsi `hitung_kata` digunakan untuk menghitung berapa kali sebuah kata tertentu muncul dalam file teks "thebeatles.txt" dan mengirimkan hasilnya melalui file descriptor. Kode dimulai dengan deklarasi dan inisialisasi variabel `filename` yang akan menyimpan nama file yang akan dibaca, serta variabel `count` yang akan digunakan untuk menghitung berapa kali kata yang dicari muncul dalam file. Selain itu, ada juga variabel `word_to_search` yang akan digunakan untuk menyimpan kata yang dicari dalam bentuk string dengan tambahan spasi di awal dan akhir, sehingga pencarian kata tersebut bisa lebih tepat.

    Selanjutnya, kode membuka file yang akan dibaca menggunakan fungsi fopen dengan mode "r" (read) dan melakukan pengecekan apakah file berhasil dibuka atau tidak. Jika file tidak dapat dibuka, program akan mencetak pesan error menggunakan perror dan keluar dari program dengan `exit(1)`.

    Setelah file berhasil dibuka, program membaca isi file karakter per karakter menggunakan loop `while`. Saat membaca karakter, program melakukan pengecekan apakah karakter tersebut adalah huruf (alphabet) menggunakan fungsi `isalpha`. Jika karakter adalah huruf, maka karakter tersebut akan dimasukkan ke dalam variabel `current_word`, yang merupakan string sementara yang sedang dibangun untuk membentuk kata yang sedang dibaca. Variabel `in_word` digunakan untuk menandai apakah sedang dalam proses membaca sebuah kata atau tidak.

    Jika karakter yang dibaca bukan huruf (misalnya spasi atau tanda baca), program akan memeriksa apakah sebelumnya sedang dalam proses membaca sebuah kata `(in_word == 1)`. Jika ya, maka kata yang telah terbentuk dalam current_word akan dibandingkan dengan kata yang ingin dicari (word). Jika kedua kata tersebut sama, maka variabel `count` akan ditambah satu, mengindikasikan bahwa kata yang dicari muncul sekali lagi. Selanjutnya, variabel `current_word` akan di-reset ke string kosong, sehingga bisa digunakan untuk membaca kata selanjutnya.

```c
void hitung_kata(const char* word) {
    char filename[MAX_FILENAME_LENGHT];
    sprintf(filename, "thebeatles.txt");

    int count = 0;
    char word_to_search[MAX_WORD_LENGHT];
    sprintf(word_to_search, " %s ", word);

    FILE *input_file = fopen(filename, "r");
    if (input_file == NULL) {
        perror("Failed to open file");
        exit(1);
    }

    int in_word = 0;
    char current_word[MAX_WORD_LENGHT] = {0};
    int word_len = 0;

    int c;
    while ((c = fgetc(input_file)) != EOF) {
        if (isalpha(c)) {
            current_word[word_len++] = c;
            in_word = 1;
        } else {
            if (in_word) {
                current_word[word_len] = '\0';
                if (strcmp(current_word, word) == 0) {
                    count++;
                }
                word_len = 0;
                in_word = 0;
            }
        }
    }

    fclose(input_file);

    char message[MAX_WORD_LENGHT * 2];
    snprintf(message, sizeof(message), "Kata '%s' muncul sebanyak %d kali dalam file '%s'", word, count, filename);
    buat_log("KATA", message);
    printf("%s\n", message);
}
```
    Setelah selesai membaca seluruh file, program akan menutup file dengan menggunakan `fclose`, dan kemudian membangun sebuah pesan yang berisi informasi tentang berapa kali kata yang dicari muncul dalam file beserta nama file tersebut. Pesan ini akan dicetak ke layar dan juga di-log ke dalam suatu fungsi yang disebut `buat_log`. Akhirnya, program akan mencetak pesan tersebut ke layar menggunakan `printf`. 

    Seluruh kode ini bekerja untuk menghitung kemunculan suatu kata dalam file "thebeatles.txt" secara case-sensitive dan mengirimkan hasil penghitungan tersebut ke file descriptor yang ditentukan, sehingga hasil penghitungan tersebut dapat digunakan dalam proses selanjutnya.

8. Kemudian kita tambahkan 1 fungsi lagi untuk melakukan penghitungan huruf yaitu dengan `hitung_huruf`. Fungsi `hitung_huruf` bertugas menghitung berapa kali sebuah huruf tertentu muncul dalam file teks "thebeatles.txt" dan mengirimkan hasilnya melalui file descriptor. Fungsi ini menerima parameter berupa pointer ke karakter `(const char* letter)`. Fungsi ini akan menghitung berapa kali huruf yang ditentukan muncul dalam sebuah file teks. Kode kemudian mendefinisikan sebuah array karakter `filename` dengan ukuran maksimum `MAX_FILENAME_LENGTH` dan menggunakan fungsi `sprintf` untuk menginisialisasi nama file yang akan dibaca, dalam hal ini adalah "thebeatles.txt".

    Selanjutnya, sebuah variabel count dideklarasikan untuk menyimpan jumlah kemunculan huruf yang dicari dalam file. Variabel `count` diinisialisasi dengan nilai 0. Program membuka file "thebeatles.txt" untuk dibaca dengan menggunakan fungsi `fopen`, dan kemudian memeriksa apakah operasi pembukaan file berhasil atau tidak. Jika operasi pembukaan file gagal (kemungkinan file tidak ditemukan), program akan mencetak pesan kesalahan menggunakan `perror` dan menghentikan eksekusi program dengan `exit(1)`.

    Selanjutnya, program membaca satu karakter pada satu waktu dari file menggunakan loop `while` dan `fgetc`. Jika karakter yang dibaca sama dengan huruf yang dicari (dalam kode, disimpan dalam variabel character), maka variabel count akan diinkrementasi, menghitung jumlah kemunculan huruf tersebut dalam file.

```c
void hitung_huruf(const char* letter, int write_fd) {
    char filename[MAX_FILENAME_LENGTH];
    sprintf(filename, "thebeatles.txt");

    int count = 0;

    FILE *input_file = fopen(filename, "r");
    if (input_file == NULL) {
        perror("Failed to open file");
        exit(1);
    }

    char character = letter[0];

    int c;
    while ((c = fgetc(input_file)) != EOF) {
        if (c == character) {
            count++;
        }
    }

    fclose(input_file);

    char message[MAX_WORD_LENGTH * 2];
    snprintf(message, sizeof(message), "Huruf '%c' muncul sebanyak %d kali dalam file '%s'", character, count, filename);
    write(write_fd, message, strlen(message) + 1);

    buat_log("HURUF", message);
}
```

    Setelah selesai membaca file, program menutup file menggunakan `fclose`.Selanjutnya, program membangun sebuah pesan yang berisi informasi tentang jumlah kemunculan huruf yang dicari dalam file beserta nama file. Pesan ini dibangun menggunakan fungsi `snprintf` dan disimpan dalam variabel `message`.

Akhirnya, pesan tersebut dicetak ke layar menggunakan printf, dan juga diproses oleh fungsi buat_log, yang mungkin digunakan untuk mencatat log aktivitas program.

9. Setelah itu kita membuat fungsi `main`. Fungsi ini berperan sebagai titik masuk utama untuk program yang melakukan analisis. Program ini memiliki dua mode operasi: mode perhitungan kata dan mode perhitungan huruf, yang tergantung pada argumen yang diberikan saat menjalankan program. Pertama, program memeriksa argumen yang diberikan oleh pengguna dalam command-line menggunakan `argc` dan `argv`. Jika jumlah argumen tidak sesuai atau argumen yang diberikan tidak valid (bukan "-kata" atau "-huruf"), maka program akan mencetak pesan penggunaan yang benar dan keluar dengan status kesalahan.

```c
 if (argc != 2 || (strcmp(argv[1], "-kata") != 0 && strcmp(argv[1], "-huruf") != 0)) {
        printf("Usage: %s <-kata|-huruf>\n", argv[0]);
        return 1;
    }
```

10. Kemudian, program memanggil fungsi `hapus_nonhuruf` untuk membersihkan teks dari karakter non-huruf dalam file "lirik.txt" dan menyimpan teks yang telah dibersihkan dalam file "thebeatles.txt".

11. Selanjutnya, kita membuat sebuah pipe menggunakan `pipe` untuk mengatur komunikasi antara proses parent dan child. Pipe ini digunakan untuk mengirim pesan hasil perhitungan dari child process ke parent process.Program selanjutnya melakukan fork menggunakan `fork()`, yang menciptakan proses child yang baru. Jika fork berhasil, maka ada dua jalur eksekusi yang berjalan secara paralel: satu untuk parent dan satu untuk child.

```c
int pipefd[2];
if (pipe(pipefd) == -1) {
    perror("Pipe creation failed");
    exit(1);
}

pid_t child_pid = fork();

if (child_pid < 0) {
    perror("Fork failed");
    exit(1);
}
```

    Dalam proses child, program menentukan mode operasi berdasarkan argumen yang diberikan oleh pengguna. Jika argumen adalah "-kata", maka program meminta pengguna untuk memasukkan kata yang ingin dihitung menggunakan `scanf`, dan kemudian memanggil fungsi `hitung_kata` untuk menghitung kemunculan kata tersebut dalam teks yang telah dibersihkan. Jika argumen adalah "-huruf", program meminta pengguna untuk memasukkan huruf yang ingin dihitung, dan kemudian memanggil fungsi `hitung_huruf` untuk menghitung kemunculan huruf tersebut dalam teks yang telah dibersihkan.

```c
if (child_pid == 0) {
        // Proses child
    close(pipefd[0]);

    if (strcmp(argv[1], "-kata") == 0) {
        char word[MAX_WORD_LENGHT];
        printf("Masukkan kata yang ingin dihitung: ");
        scanf("%s", word);
        hitung_kata(word);
    } else if (strcmp(argv[1], "-huruf") == 0) {
        char letter[MAX_WORD_LENGHT];
        printf("Masukkan huruf yang ingin dihitung: ");
        scanf("%s", letter);
        hitung_huruf(letter);
    }

    close(pipefd[1]);
    exit(0);
} else {
    close(pipefd[1]);
    int status;
    wait(&status);
}
```

    Setelah proses child selesai, pipe ditutup menggunakan `close(pipefd[1])` untuk mengakhiri proses penulisan pesan hasil perhitungan ke pipe. Proses child juga keluar dengan `exit(0)`. Proses parent menunggu proses child selesai menggunakan `wait` dan kemudian mengakhiri program.

12. Secara keseluruhan, fungsi main mengatur alur program, memproses argumen, membersihkan teks, menciptakan komunikasi antara parent dan child menggunakan pipe, dan menjalankan operasi perhitungan kata atau huruf sesuai dengan mode yang dipilih oleh pengguna.Berikut kode lengkapnya:

```c
int main(int argc, char *argv[]) {
    if (argc != 2 || (strcmp(argv[1], "-kata") != 0 && strcmp(argv[1], "-huruf") != 0)) {
        printf("Usage: %s <-kata|-huruf>\n", argv[0]);
        return 1;
    }

    hapus_nonhuruf();

    int pipefd[2];
    if (pipe(pipefd) == -1) {
        perror("Pipe creation failed");
        exit(1);
    }

    pid_t child_pid = fork();

    if (child_pid < 0) {
        perror("Fork failed");
        exit(1);
    }

    if (child_pid == 0) {
        // Proses child
        close(pipefd[0]);

        if (strcmp(argv[1], "-kata") == 0) {
            char word[MAX_WORD_LENGHT];
            printf("Masukkan kata yang ingin dihitung: ");
            scanf("%s", word);
            hitung_kata(word);
        } else if (strcmp(argv[1], "-huruf") == 0) {
            char letter[MAX_WORD_LENGHT];
            printf("Masukkan huruf yang ingin dihitung: ");
            scanf("%s", letter);
            hitung_huruf(letter);
        }

        close(pipefd[1]);
        exit(0);
    } else {
        close(pipefd[1]);
        int status;
        wait(&status);
    }

    return 0;
}
```

**B. HASIL**
1. Berikut hasil output dari file thebeatles.txt yang muncul setelah `./8ballondors -(argumen kata/ huruf)` dijalankan

```
Sudah kudaki gunung tertinggi hanya untuk mencari di mana dirimu Sudah kujelajahi isi bumi hanya untuk dapat hidup bersamamu Sudah kuarungi laut samudera hanya untuk mencari tempat berlabuhmu Tapi semakin jauh ku mencari cinta semakin aku tak mengerti Akulah Arjuna yang mencari cinta Wahai wanita cintailah aku Mungkin kutemui cinta sejati saat aku hembuskan nafas terakhirku Mungkin cinta sejati memang tak ada dalam cerita kehidupan ini Akulah Arjuna yang mencari cinta Wahai wanita cintailah aku Akulah Arjuna yang mencari cinta Wahai wanita cintailah aku Akulah Arjuna yang mencari cinta Wahai wanita cintailah aku Usap air matamu yang menetes di pipimu ku pastikan semuanya akan baikbaik saja bila kau terus pandangi langit tinggi di angkasa Tak kan ada habisnya segala hasrat di dunia hawa tercipta di dunia untuk menemani sang Adam begitu juga dirimu tercipta tuk temani aku Renungkan sejenak arti hadirku di sini jangan pernah ingkari dirimu adalah wanita Harusnya dirimu menjadi perhiasan sangkar maduku walaupun kadang diriku bertekuk lutut di hadapanmu Hawa tercipta di dunia untuk menemani sang Adam begitu juga dirimu tercipta tuk temani aku Harusnya dirimu menjadi perhiasan sangkar maduku walaupun kadang diriku bertekuk lutut di hadapanmu Hawa tercipta di dunia untuk menemani sang Adam begitu juga dirimu tercipta tuk temani aku Bukalah pintu jiwamu dengar bisikan sanubari Semua adalah isyarat isyarat dari Sang Pencipta Di saat terlihat bintangbintang atau malam yang gelap gulita pada saat semangatmu hilang carilah orang lain dan bicara Manusia tidaklah pernah ditinggal sendirian saja Terkadang kita tak menyadari hal baik disekitar kita Hidup bagaikan pesawat kertas terbang dan membawa cinta kita semua Sayap yang terbentang dengan percaya diri dilihat semua orang walau tak tahu cara melipatnya Suatu saat pasti akan berhasil lalu terbang Kekuatan harapan yang menerbangkannya Ya mari nikmatilah Sanbyaku rokujugo nichiHidup bagaikan pesawat kertas terbang dan pergi membawa impian Sekuat tenaga dengan hembusan angin terus melaju terbang Jangan bandingkan jarak terbangnya tapi bagaimana dan apa yang dilalui karena itulah satu hal yang penting Selalu sesuai kata hati Sanbyaku rokujugo nichi Ayo terbanglah Coba terbanglah ayo terbanglah Coba terbanglah ayo terbanglah Coba terbanglah
```

2. Ketika file thebeatles.txt muncul, maka program akan memerintahkan kita untuk memasukkan kata/huruf yang ingin dihitung. Kita mulai dengan menjalankan `./8ballondors -kata`. Maka hasilnya sebagai berikut:

![kata](/uploads/adb4f29828394978886e4397e67a768b/kata.png)

- Jumlahnya sudah sama dengan file thebeatles.txt

![katayang](/uploads/1988841f7872a9f9a57bbaf473d4bd98/katayang.png)

3. Selanjutnya kita menjalankan `./8ballondors -huruf`. Maka hasilnya sebagai berikut:

![huruf](/uploads/c7db6db746a7e2f50ff1b39d49ed4b1c/huruf.png)

- Jumlahnya sudah sama dengan file thebeatles.txt

![hurufa](/uploads/9e87c438c97d2a32a02ba7b22003c317/hurufa.png)

4. Menjalankan `./8ballondors -huruf` dan menginputkan huruf besar:

![hurufabesar](/uploads/26af314adf04e23c14701aca87f7620f/hurufabesar.png)

- Jumlahnya sudah sama dengan file thebeatles.txt

![kataabesar](/uploads/e97afd487953946b9bbb8617c05002f7/kataabesar.png)

4.  Berikut hasil pencatatan pencarian yang ada di file `frekuensi.log`:

![frekuensi1](/uploads/1ac55a451a88085967a280d81f1db649/frekuensi1.png)

5. Kurang lebih struktur foldernya seperti ini:

![tree](/uploads/5ad874eb974e85a5a3a933e13de3609f/tree.png)

**C. REVISI**

Pada kode sebelumnya masih terdapat 1 pipe saja, sedangkan pada soal diperintahkan wajib menggunakan 2 pipes,sehingga kita harus mengkonfigurasi ulang kodenya dengan 2 pipes. Berikut kode perbandingan kode hasil revisi secara keseluruhan:

```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <ctype.h>

#define MAX_WORD_LENGTH 100
#define MAX_FILENAME_LENGTH 100
#define LOG_FILENAME "frekuensi.log"

void buat_log(const char* type, const char* message) {
    time_t now;
    time(&now);
    char timestamp[20];
    strftime(timestamp, 20, "[%d/%m/%y %H:%M:%S]", localtime(&now));
    FILE* log_file = fopen(LOG_FILENAME, "a");
    if (log_file) {
        fprintf(log_file, "%s [%s] %s\n", timestamp, type, message);
        fclose(log_file);
    }
}

void hapus_nonhuruf() {
    FILE *input_file = fopen("lirik.txt", "r");
    FILE *output_file = fopen("thebeatles.txt", "w");

    if (input_file == NULL || output_file == NULL) {
        perror("Failed to open file");
        exit(1);
    }

    int c;
    while ((c = fgetc(input_file)) != EOF) {
        if (isalpha(c) || c == ' ' || c == '\n') {
            fputc(c, output_file);
        }
    }

    fclose(input_file);
    fclose(output_file);
}

void hitung_kata(const char* word, int write_fd) {
    char filename[MAX_FILENAME_LENGTH];
    sprintf(filename, "thebeatles.txt");

    int count = 0;
    char word_to_search[MAX_WORD_LENGTH];
    sprintf(word_to_search, " %s ", word);

    FILE *input_file = fopen(filename, "r");
    if (input_file == NULL) {
        perror("Failed to open file");
        exit(1);
    }

    int in_word = 0;
    char current_word[MAX_WORD_LENGTH] = {0};
    int word_len = 0;

    int c;
    while ((c = fgetc(input_file)) != EOF) {
        if (isalpha(c)) {
            current_word[word_len++] = c;
            in_word = 1;
        } else {
            if (in_word) {
                current_word[word_len] = '\0';
                if (strcmp(current_word, word) == 0) {
                    count++;
                }
                word_len = 0;
                in_word = 0;
            }
        }
    }

    fclose(input_file);

    char message[MAX_WORD_LENGTH * 2];
    snprintf(message, sizeof(message), "Kata '%s' muncul sebanyak %d kali dalam file '%s'", word, count, filename);
    write(write_fd, message, strlen(message) + 1);

    buat_log("KATA", message);
}

void hitung_huruf(const char* letter, int write_fd) {
    char filename[MAX_FILENAME_LENGTH];
    sprintf(filename, "thebeatles.txt");

    int count = 0;

    FILE *input_file = fopen(filename, "r");
    if (input_file == NULL) {
        perror("Failed to open file");
        exit(1);
    }

    char character = letter[0];

    int c;
    while ((c = fgetc(input_file)) != EOF) {
        if (c == character) {
            count++;
        }
    }

    fclose(input_file);

    char message[MAX_WORD_LENGTH * 2];
    snprintf(message, sizeof(message), "Huruf '%c' muncul sebanyak %d kali dalam file '%s'", character, count, filename);
    write(write_fd, message, strlen(message) + 1);

    buat_log("HURUF", message);
}

int main(int argc, char *argv[]) {
    if (argc != 2 || (strcmp(argv[1], "-kata") != 0 && strcmp(argv[1], "-huruf") != 0)) {
        printf("Usage: %s <-kata|-huruf>\n", argv[0]);
        return 1;
    }

    hapus_nonhuruf();

    int pipefd_kata[2];
    int pipefd_huruf[2];

    if (pipe(pipefd_kata) == -1 || pipe(pipefd_huruf) == -1) {
        perror("Pipe creation failed");
        exit(1);
    }

    pid_t child_pid = fork();

    if (child_pid < 0) {
        perror("Fork failed");
        exit(1);
    }

    if (child_pid == 0) {
        // Child
        close(pipefd_kata[0]);
        close(pipefd_huruf[0]);

        if (strcmp(argv[1], "-kata") == 0) {
            char word[MAX_WORD_LENGTH];
            printf("Masukkan kata yang ingin dihitung: ");
            scanf("%s", word);
            hitung_kata(word, pipefd_kata[1]);
        } else if (strcmp(argv[1], "-huruf") == 0) {
            char letter[MAX_WORD_LENGTH];
            printf("Masukkan huruf yang ingin dihitung: ");
            scanf("%s", letter);
            hitung_huruf(letter, pipefd_huruf[1]);
        }

        close(pipefd_kata[1]);
        close(pipefd_huruf[1]);
        exit(0);
    } else {
        // Parents
        close(pipefd_kata[1]);
        close(pipefd_huruf[1]);

        char result_buffer[MAX_WORD_LENGTH * 2];

        if (strcmp(argv[1], "-kata") == 0) {
            read(pipefd_kata[0], result_buffer, sizeof(result_buffer));
        } else if (strcmp(argv[1], "-huruf") == 0) {
            read(pipefd_huruf[0], result_buffer, sizeof(result_buffer));
        }

        printf("%s\n", result_buffer);

        int status;
        wait(&status);
    }

    return 0;
}

```

Untuk mengetahui perbedaan kode seebelum dan sesudah revisi, berikut penjelasan perbandingannya. Antara kode yang sudah menggunakan 2 pipes dengan kode sebelumnya menggunakan 2 pipes.

1. Penanganan Output dan Komunikasi:

- Pada program sebelum revisi, hanya ada satu set pipe `(pipefd)` yang digunakan untuk mengkomunikasikan hasil perhitungan, dan pemrosesan berdasarkan apakah argumen adalah -kata atau -huruf diatur dalam proses child. Proses child menghasilkan output (jumlah kata atau huruf) dan mengirimkannya ke proses parent melalui pipe.

```c
int pipefd[2];
if (pipe(pipefd) == -1) {
    perror("Pipe creation failed");
    exit(1);
}

```

- Pada program setelah revisi, terdapat dua set pipe terpisah, yaitu `pipefd_kata` dan `pipefd_huruf`, yang masing-masing digunakan untuk mengkomunikasikan hasil perhitungan kata dan huruf. Proses child menerima input dari pengguna dan melakukan perhitungan, kemudian mengirimkan hasilnya ke proses parent melalui salah satu dari dua pipe sesuai dengan argumen yang diberikan.

```c
int pipefd_kata[2];
int pipefd_huruf[2];

if (pipe(pipefd_kata) == -1 || pipe(pipefd_huruf) == -1) {
    perror("Pipe creation failed");
    exit(1);
}
```

2. Penanganan Hasil Perhitungan:

- Pada program sebelum revisi, hasil perhitungan kata atau huruf diambil oleh proses parent dari pipe sesuai dengan argumen yang diberikan. Hasil perhitungan diambil menggunakan read dari `pipefd` yang sesuai dan kemudian dicetak di layar.

```c
// Untuk mengambil hasil perhitungan kata
read(pipefd[0], result_buffer, sizeof(result_buffer));
```

- Pada program setelah revisi, hasil perhitungan kata atau huruf diambil oleh proses parent dengan cara yang lebih terstruktur. Parent memeriksa argumen yang diberikan untuk menentukan apakah harus membaca dari `pipefd_kata` atau `pipefd_huruf`. Ini membuat kode lebih jelas dan memisahkan logika perhitungan dan komunikasi.

```c
// Untuk mengambil hasil perhitungan kata
read(pipefd_kata[0], result_buffer, sizeof(result_buffer));
```

3. Fungsi hitung_kata dan hitung_huruf:

- Pada program sebelum revisi, fungsi `hitung_kata` dan `hitung_huruf` hanya menghitung kata atau huruf dan mencetak hasilnya ke layar.

```c
void hitung_kata(const char* word) {
    // ...
}

void hitung_huruf(const char* letter) {
    // ...
}
```

- Pada program setelah revisi, fungsi `hitung_kata` dan `hitung_huruf` menerima tambahan parameter, yaitu file descriptor untuk pipe. Hasil perhitungan kata atau huruf dikirim melalui pipe ke proses parent untuk kemudian dicetak di sana.

```c
void hitung_kata(const char* word, int write_fd) {
    // ...
}

void hitung_huruf(const char* letter, int write_fd) {
    // ...
}
```

4. Penutupan Pipe:

- Pada program sebelum revisi, menutup semua file descriptor pipe dalam proses child.

```c
// Di dalam proses child
close(pipefd[0]);
```

- Pada program setelah revisi, juga menutup semua file descriptor pipe dalam proses child. Namun, parent dan child kini memiliki dua set pipe yang berbeda, yaitu `pipefd_kata` dan `pipefd_huruf`, sehingga keduanya harus ditutup.

```c
// Di dalam proses child
close(pipefd_kata[1]);
close(pipefd_huruf[1]);
```

5. 6. Kode Interaksi dengan Pengguna:

- Pada program sebelum revisi, interaksi dengan pengguna untuk memasukkan kata atau huruf dilakukan dalam proses child.

```c
if (strcmp(argv[1], "-kata") == 0) {
    char word[MAX_WORD_LENGHT];
    printf("Masukkan kata yang ingin dihitung: ");
    scanf("%s", word);
    hitung_kata(word);
} else if (strcmp(argv[1], "-huruf") == 0) {
    char letter[MAX_WORD_LENGHT];
    printf("Masukkan huruf yang ingin dihitung: ");
    scanf("%s", letter);
    hitung_huruf(letter);
}
```

- Pada program setelah revisi, interaksi dengan pengguna untuk memasukkan kata atau huruf juga dilakukan dalam proses child. Namun, pesan hasil perhitungan ditampilkan di proses parent setelah menerima data melalui pipe.

```c
if (strcmp(argv[1], "-kata") == 0) {
    char word[MAX_WORD_LENGTH];
    printf("Masukkan kata yang ingin dihitung: ");
    scanf("%s", word);
    hitung_kata(word, pipefd_kata[1]);
} else if (strcmp(argv[1], "-huruf") == 0) {
    char letter[MAX_WORD_LENGTH];
    printf("Masukkan huruf yang ingin dihitung: ");
    scanf("%s", letter);
    hitung_huruf(letter, pipefd_huruf[1]);
}
```

---


### **III. SOAL 3<a name="soal3"></a>**
**A. PEMBAHASAN**

Christopher adalah seorang praktikan sisop, dia mendapat tugas dari pak Nolan untuk membuat komunikasi antar proses dengan menerapkan konsep message queue. Pak Nolan memberikan kredensial list  users yang harus masuk ke dalam program yang akan dibuat. Lebih lanjutnya pak Nolan memberikan instruksi tambahan sebagai berikut : 

**POIN A.** Bantulah Christopher untuk membuat program tersebut, dengan menerapkan konsep message queue(wajib) maka buatlah 2  program, sender.c sebagai pengirim dan receiver.c sebagai penerima. Dalam hal ini, sender hanya dapat mengirimkan perintah berupa STRING ke sistem dan semua aktivitas sesuai perintah akan dikerjakan oleh receiver.
Struktur foldernya akan menjadi seperti berikut

```
└── soal3
	├── users
	├── receiver.c
	└── sender.c
```
1. Penjelasan pada bagian receiver.c
```c
#define MAX_MSG_SIZE 256
#define AUTHENTICATION_QUEUE_KEY 12345
#define TRANSFER_QUEUE_KEY 54321
#define CREDS_QUEUE_KEY 98765

struct Message {
    long mtype;
    char mtext[MAX_MSG_SIZE];
};
```
Pada bagian ini, didefinisikan beberapa konstanta seperti ukuran maksimum pesan dan kunci unik untuk message queue. Struktur `Message` digunakan untuk menyimpan pesan yang akan dikirim melalui antrean pesan. Ini memiliki dua anggota: `mtype` untuk tipe pesan dan `mtext` untuk isi pesan.
```c
int main() {
    int auth_queue_id = msgget(AUTHENTICATION_QUEUE_KEY, IPC_CREAT | 0666);
    int transfer_queue_id = msgget(TRANSFER_QUEUE_KEY, IPC_CREAT | 0666);
    int creds_queue_id = msgget(CREDS_QUEUE_KEY, IPC_CREAT | 0666);

    if (auth_queue_id == -1 || transfer_queue_id == -1) {
        perror("Failed to create message queues");
        exit(1);
    }

    pid_t auth_pid = fork();
    if (auth_pid == -1) {
        perror("Failed to fork authentication process");
        exit(1);
    } else if (auth_pid == 0) {
        authenticationProcess(auth_queue_id);
        exit(0);
    }

    pid_t transfer_pid = fork();
    if (transfer_pid == -1) {
        perror("Failed to fork file transfer process");
        exit(1);
    } else if (transfer_pid == 0) {
        fileTransferProcess(transfer_queue_id);
        exit(0);
    }

    pid_t creds_pid = fork();
    if (creds_pid == -1) {
        perror("Failed to fork file CREDS process");
        exit(1);
    } else if (creds_pid == 0) {
        processCredsCommand(creds_queue_id);
        exit(0);
    }

    waitpid(auth_pid, NULL, 0);

    msgctl(auth_queue_id, IPC_RMID, NULL);
    msgctl(transfer_queue_id, IPC_RMID, NULL);

    return 0;
}
```
Fungsi `main` adalah titik awal eksekusi program, dan di dalamnya, program melakukan beberapa langkah kunci. Pertama, program menciptakan tiga antrean pesan menggunakan `msgget` untuk memfasilitasi komunikasi antara proses-proses. Selanjutnya, program melakukan fork untuk tiga proses anak yang berbeda, yaitu `authenticationProcess`, `fileTransferProcess`, dan `processCredsCommand`, masing-masing bertanggung jawab untuk menghandle permintaan otentikasi, transfer file, dan perintah "CREDS". Fungsi main ini memainkan peran kunci dalam menginisialisasi dan mengelola seluruh aplikasi server yang dapat mengatasi otentikasi, transfer file, dan perintah "CREDS" dengan berkomunikasi melalui antrean pesan.

2. Penjelasan pada bagian sender.c
```c
#define AUTHENTICATION_QUEUE_KEY 12345
#define TRANSFER_QUEUE_KEY 54321
#define CREDS_QUEUE_KEY 98765

// Structure for message content
struct Message {
    long mtype;
    char mtext[256];
};
```
Bagian ini mendefinisikan konstanta dan struktur yang sama dengan receiver.c.

**POIN B.** Ternyata list kredensial yang diberikan Pak Nolan semua passwordnya terenkripsi dengan menggunakan base64. Untuk mengetahui kredensial yang valid maka Sender pertama kali akan mengirimkan perintah CREDS kemudian sistem receiver akan melakukan decrypt/decode/konversi pada file users, lalu menampilkannya pada receiver. Hasilnya akan menjadi seperti berikut : 
```
./receiver
Username: Mayuri, Password: TuTuRuuu
Username: Onodera, Password: K0sak!
Username: Johan, Password: L!3b3rt
Username: Seki, Password: Yuk!n3
Username: Ayanokouji, Password: K!yot4kA
….
```
1. Penjelasan pada bagian receiver.c
```c
char* decryptBase64(const char* base64Input) {
    size_t length = strlen(base64Input);
    int decodedLength = length;

    char* decodedText = (char*)malloc(decodedLength);
    if (!decodedText) {
        perror("Memory allocation error");
        exit(1);
    }

    if (EVP_DecodeBlock((unsigned char*)decodedText, (const unsigned char*)base64Input, length) == -1) {
        perror("Base64 decoding error");
        free(decodedText);
        exit(1);
    }
    decodedText[length] = '\0';

    return decodedText;
}
```
Fungsi `decryptBase64` digunakan untuk mendekripsi input dalam format Base64 menggunakan OpenSSL dengan EVP. Ini mengalokasikan memori untuk teks yang didekripsi dan kemudian menggunakan OpenSSL untuk mendekripsinya.

```c
void processCredsCommand(int creds_queue_id) {
    while (true) {
        struct Message msg;
        msgrcv(creds_queue_id, &msg, sizeof(msg.mtext), 1, 0);

        char* command = strtok(msg.mtext, " ");

        if (strncmp(command, "CREDS", 5) == 0) {
            FILE *users_file = fopen("users/users.txt", "r");

            if (users_file == NULL) {
                perror("fopen");
                exit(1);
            }

            char line[MAX_MSG_SIZE];
            while (fgets(line, sizeof(line), users_file)) {
                char *username = strtok(line, ":");
                char *password = strtok(NULL, ":");
                char decrypted_password[MAX_MSG_SIZE];

                if (password != NULL) {
                    password[strcspn(password, "\n")] = '\0';
                    char *decoded_password = decryptBase64(password);

                    printf("Username: %s, Password: %s\n", username, decoded_password);
                }
            }

            fclose(users_file);
        }
    }
}
```
Fungsi `processCredsCommand` bertanggung jawab untuk menangani perintah `CREDS` yang dikirimkan melalui antrean pesan. Proses ini membaca file "users.txt", yang berisi entri pengguna dan kata sandi terenkripsi dalam format Base64, dan mendekripsi serta mencetak informasi pengguna ke konsol. Fungsi ini memisahkan setiap baris dalam file menjadi nama pengguna dan kata sandi, dan kemudian mendekripsi kata sandi menggunakan fungsi `decryptBase64`. Hasilnya adalah mencetak username dan kata sandi terdekripsi ke konsol. Ini berguna untuk memantau dan memeriksa kata sandi yang telah didekripsi dari entri pengguna dalam file konfigurasi.

2. Penjelasan pada bagian sender.c
```c
// Menggunakan system dan wget untuk mengunduh folder "users" dari tautan
char wgetCommand[256];
sprintf(wgetCommand, "wget --quiet --no-check-certificate 'https://drive.google.com/uc?id=1CrERpikZwuxgAwDvrhRnB2VyAYtT2SIf&export=download' -O users.zip");
system(wgetCommand);

// Menggunakan system untuk mengekstrak "users.txt" dari arsip ke direktori "users"
system("unzip -oj users.zip -d users");
system("rm -r users.zip");
```
Program menggunakan perintah sistem `(system)` untuk mengunduh arsip "users.zip" yang berisi file "users.txt" dari tautan yang disediakan.
Program mengekstrak "users.txt" dari arsip tersebut dan menyimpannya dalam direktori "users". Kemudian, arsip "users.zip" dihapus.
```c
if (strcmp(command, "CREDS") == 0) {
    if (msgsnd(creds_queue_id, &msg, sizeof(msg.mtext), 0) == -1) {
        perror("Failed to send CREDS command to the message queue");
    } else {
        creds_sent = 1;
        continue;
    }
}
```
Ketika pengguna memasukkan "CREDS", program mengirim pesan dengan perintah tersebut ke antrean pesan "CREDS". Jika pengiriman berhasil, variabel `creds_sent` disetel menjadi 1 untuk menandakan bahwa perintah "CREDS" telah dikirim.

**POIN C.** Setelah mengetahui list kredensial, bantulah christopher untuk membuat proses autentikasi berdasarkan list kredensial yang telah disediakan. Proses autentikasi dilakukan dengan menggunakan perintah AUTH: username password kemudian jika proses autentikasi valid dan berhasil maka akan menampilkan Authentication successful . Authentication failed jika gagal.
1. Penjelasan pada bagian receiver.c
```c
bool authenticateUser(const char* username, const char* providedPassword) {
    FILE* file = fopen("users/users.txt", "r");
    if (file) {
        char line[256];

        while (fgets(line, sizeof(line), file)) {
            char* colon_position = strchr(line, ':');
            if (colon_position != NULL) {
                *colon_position = '\0';
                char* storedUsername = line;
                char* base64Creds = colon_position + 1;
                base64Creds[strcspn(base64Creds, "\r\n")] = '\0';
                char* decryptedPassword = decryptBase64(base64Creds);

                if (strcmp(username, storedUsername) == 0 && strcmp(providedPassword, decryptedPassword) == 0) {
                    free(decryptedPassword);
                    fclose(file);
                    return true;
                }

                free(decryptedPassword);
            }
        }

        fclose(file);
    }

    return false;
}
```
Fungsi `authenticateUser` bertanggung jawab untuk mengotentikasi pengguna dengan memeriksa apakah pasangan nama pengguna dan kata sandi yang diberikan oleh klien cocok dengan entri yang ada dalam file "users.txt". Dalam proses ini, fungsi membuka file konfigurasi "users.txt" dan membaca setiap baris, memisahkan nama pengguna dan kata sandi terenkripsi yang dipisahkan oleh titik dua (":"). Selanjutnya, kata sandi terenkripsi ini didekripsi menggunakan fungsi `decryptBase64`. Jika pasangan nama pengguna dan kata sandi yang diberikan oleh klien cocok dengan data yang ada dalam file dan kata sandi terdekripsi, maka fungsi mengembalikan `true`, menandakan bahwa autentikasi berhasil. Jika tidak ada pasangan yang cocok atau kesalahan lain terjadi, maka fungsi mengembalikan `false`, menunjukkan bahwa autentikasi gagal. Selain itu, fungsi memastikan untuk membebaskan memori yang dialokasikan untuk kata sandi terdekripsi dan menutup file setelah selesai digunakan.
```c
void authenticationProcess(int auth_queue_id) {
    while (true) {
        struct Message msg;
        msgrcv(auth_queue_id, &msg, sizeof(msg.mtext), 1, 0);

        char* command = strtok(msg.mtext, " ");
        char* username = strtok(NULL, " ");
        char* password = strtok(NULL, " ");

        if (command != NULL && username != NULL && password != NULL && strcmp(command, "AUTH:") == 0) {
            bool isAuthenticated = authenticateUser(username, password);

            if (isAuthenticated) {
                strcpy(msg.mtext, "Authentication successful");
            } else {
                strcpy(msg.mtext, "Authentication failed");
            }

            msgsnd(auth_queue_id, &msg, sizeof(msg.mtext), 0);
            printf("Server (AUTH): %s\n", msg.mtext);

            kill(receiver_pid, SIGTERM);
            exit(0);
        }
    }
}
```
Fungsi `authenticationProcess` berperan sebagai proses yang menangani permintaan otentikasi dari klien melalui penggunaan antrean pesan. Dalam proses ini, fungsi terus-menerus menunggu dan menerima pesan otentikasi dari antrean pesan dengan menggunakan `msgrcv`. Pesan otentikasi tersebut kemudian dipisahkan menjadi tiga bagian, yaitu perintah, nama pengguna, dan kata sandi, menggunakan fungsi `strtok`. Fungsi ini memeriksa apakah perintah yang diberikan adalah "AUTH:". Jika iya, maka akan memanggil fungsi `authenticateUser` untuk mengautentikasi pengguna dengan nama pengguna dan kata sandi yang diberikan. Hasil autentikasi tersebut digunakan untuk mengganti pesan yang akan dikirimkan kembali ke klien, yang kemudian disimpan kembali dalam `msg.mtext`. Setelah itu, pesan yang telah diperbarui dikirim kembali ke antrean pesan menggunakan `msgsnd`, dan pesan status otentikasi ("Authentication successful" atau "Authentication failed") dicetak ke konsol. Terakhir, proses ini menghentikan proses `receiver_pid` dengan menggunakan `kill` dan mengakhiri dirinya sendiri jika autentikasi gagal.

2. Penjelasan pada bagian sender.c
```c
if (strncmp(command, "AUTH:", 5) == 0) {
    if (creds_sent) {
        if (msgsnd(auth_queue_id, &msg, sizeof(msg.mtext), 0) == -1) {
            perror("Failed to send authentication command to the message queue");
        } else {
            // Wait for a response from the server
            if (msgrcv(auth_queue_id, &msg, sizeof(msg.mtext), 1, 0) != -1) {
                if (strcmp(msg.mtext, "Authentication successful") == 0) {
                    printf("Server (AUTH): %s\n", msg.mtext);
                    authenticated = 1;  // Set authentication status to success
                } else {
                    printf("Server (AUTH): %s\n", msg.mtext);
                    exit(1); // Keluar dari program saat otentikasi gagal
                }
            } else {
                perror("Failed to receive response from the server");
                exit(1); // Keluar dari program jika ada kesalahan menerima pesan
            }
        }
    } else {
        printf("You must run CREDS before AUTH.\n");
    }
}
```
Jika pengguna memasukkan perintah "AUTH: username password" setelah perintah "CREDS" berhasil dikirim, program akan memeriksa apakah perintah "CREDS" telah dikirim sebelumnya (dengan melihat variabel `creds_sent`). Jika ya, program mengirim perintah otentikasi ke antrean pesan otentikasi dan menunggu respon dari server. Jika otentikasi berhasil, status otentikasi `(authenticated)` disetel menjadi 1; jika gagal, program keluar.

**POIN D&E.** Setelah berhasil membuat proses autentikasi, buatlah proses transfer file. Transfer file dilakukan dari direktori Sender dan dikirim ke direktori Receiver . Proses transfer dilakukan dengan menggunakan perintah TRANSFER filename . Struktur direktorinya akan menjadi seperti berikut:

```
└── soal3
       ├── Receiver
       ├── Sender
       ├── receiver.c
       └── sender.c
```
Karena takut memorinya penuh, Christopher memberi status size(kb) pada setiap pengiriman file yang berhasil.

1. Penjelasan pada receiver.c
```c
void fileTransferProcess(int transfer_queue_id) {
    while (true) {
        struct Message msg;
        msgrcv(transfer_queue_id, &msg, sizeof(msg.mtext), 1, 0);

        char* command = strtok(msg.mtext, " ");
        char* filename = strtok(NULL, " ");

        if (strcmp(command, "TRANSFER") == 0 && filename != NULL) {
            // Implementasi transfer file
            // ...
            msgsnd(transfer_queue_id, &msg, sizeof(msg.mtext), 0);
            printf("Server (TRANSFER): %s\n", msg.mtext);
        }
    }
}
```
Fungsi `fileTransferProcess` adalah proses yang bertanggung jawab untuk mengelola permintaan transfer file dari klien melalui antrean pesan. Proses ini secara terus-menerus menunggu pesan transfer file yang dikirim ke antrean pesan dengan menggunakan `msgrcv`. Pesan tersebut kemudian diurai menjadi dua bagian, yaitu perintah dan nama file yang akan ditransfer. Fungsi ini memeriksa apakah perintah yang diberikan adalah "TRANSFER" dan apakah nama file tidak kosong. Jika kondisi ini terpenuhi, maka proses akan menjalankan implementasi transfer file yang sesuai (tidak ditampilkan dalam potongan kode). Setelah selesai, pesan yang berisi status transfer (misalnya, "File transfer successful") akan dikirimkan kembali ke antrean pesan menggunakan `msgsnd`. Selain itu, pesan status ini juga akan dicetak ke konsol dengan informasi tambahan seperti ukuran file yang berhasil ditransfer.

2. Penjelasan pada bagian sender.c
```c
if (authenticated && strncmp(command, "TRANSFER", 8) == 0) {
    // Check if "Sender" and "Receiver" directories exist, if not, create them
    struct stat st = {0};
    if (stat("Sender", &st) == -1) {
        mkdir("Sender", 0700); // Create "Sender" directory with read, write, and execute permissions
    }
    if (stat("Receiver", &st) == -1) {
        mkdir("Receiver", 0700); // Create "Receiver" directory with read, write, and execute permissions
    }

    if (msgsnd(transfer_queue_id, &msg, sizeof(msg.mtext), 0) == -1) {
        perror("Failed to send file transfer command to the message queue");
    } else {
        // Wait for the status message from the server
        if (msgrcv(transfer_queue_id, &msg, sizeof(msg.mtext), 1, 0) != -1) {
            printf("Server (TRANSFER): %s\n", msg.mtext); // Print the file size status message
        } else {
            perror("Failed to receive status message from the server");
        }
    }
}
```
Jika pengguna memasukkan perintah "TRANSFER filename" setelah berhasil terotentikasi, program akan memeriksa apakah direktori "Sender" dan "Receiver" ada atau tidak. Jika tidak ada, program akan membuat direktori tersebut. Kemudian, program mengirim perintah transfer file ke antrean pesan transfer file dan menunggu pesan status dari server yang mengindikasikan berhasil atau gagalnya transfer file. Disini dilakukan juga perhitungan size file yang akan tercetak saat status transfer file success.


**POIN F.** Apabila perintahnya tidak dapat dipahami oleh sistem, sistem akan menampilkan "UNKNOWN COMMAND".
Penjelasan pada bagian sender.c
```c
} else {
    printf("UNKNOWN COMMAND.\n");
}
```
Jika pengguna memasukkan perintah yang tidak dikenali, program mencetak pesan "UNKNOWN COMMAND".

**B. OUTPUT**
1. Isi directory untuk soal 3 setelah melakukan compile untuk receiver.c dan sender.c menggunakan `gcc receiver.c -o receiver -lssl -lcrypto` dan `gcc sender.c -o sender -lssl -lcrypto`.

![3-1](/uploads/44bff7529076be9774af9a1d46158346/3-1.jpg)

2. Menjalankan sender dengan `./sender` dan sender akan mengunduh folder users yang berisi users.txt.

![3-2_download_users](/uploads/e8bf41e413b327b78fddadda91831c24/3-2_download_users.jpg)

3. Isi directory untuk soal 3 ketika folder users berhasil diunduh.

![3-3_folder_users_muncul](/uploads/71e756aeb998ff3d5d71b9db9077967c/3-3_folder_users_muncul.jpg)

4. Menjalankan receiver dengan `./receiver`.

![3-4_receiver_kosong](/uploads/98e6f37f7cf6e3aa2f9926424b4f257b/3-4_receiver_kosong.jpg)

5. Menginput perintah `CREDS` pada terminal sender.

![3-5_CREDS_sender](/uploads/cecd15791f8e5e7b46dfa8d3e2ec2218/3-5_CREDS_sender.jpg)

6. Perintah `CREDS` dieksekusi dan menampilkan username serta password yang telah didekripsi pada terminal receiver.

![3-5_CREDS_receiver](/uploads/1d5e59292e99315e192b99828d9ca725/3-5_CREDS_receiver.jpg)

7. Menginput perintah `AUTH` dengan username/password yang salah pada terminal sender.

![3-6_AUTH_failed_sender](/uploads/fea43fd933f4067f0205e1cc502a920c/3-6_AUTH_failed_sender.jpg)

8. Output pada terminal receiver ketika authentication failed.

![3-6_AUTH_failed_receiver](/uploads/2ebc91c3f073457ab2e53f0b4be77d67/3-6_AUTH_failed_receiver.jpg)

9. Menginput perintah `AUTH` dengan username dan password yang benar pada terminal sender.

![3-6_AUTH_succes_sender](/uploads/cac38dc332553d22c4a6751df3db228d/3-6_AUTH_succes_sender.jpg)

10. Output pada terminal receiver ketika authentication success.

![3-6_AUTH_success_receiver](/uploads/faefa176cd3d3caeea603acabf3e93ee/3-6_AUTH_success_receiver.jpg)

11. Kondisi ketika perintah `AUTH` diinput sebelum `CREDS`.

![3-6_AUTH_harus_CREDS_dulu](/uploads/544dbdff41738ec582ca8ba4729ec481/3-6_AUTH_harus_CREDS_dulu.jpg)

12. Menginput perintah `TRANSFER` setelah authentication success pada terminal sender.

![3-7_TRANSFER_kosong](/uploads/798819ee09d6b87896429a5c7c6d0698/3-7_TRANSFER_kosong.jpg)

Pada inputan pertama, perintah `TRANSFER` baru hanya memproses pembuatan folder Sender dan Receiver yang masih kosong.

13. Isi directory untuk soal 3 setelah perintah `TRANSFER` dieksekusi.

![3-7_folder_transfer_muncul](/uploads/02700404a3beaf78a2038b3d46684b1b/3-7_folder_transfer_muncul.jpg)

14. Membuat sample file di dalam folder Sender.

![3-7_file_tes.txt](/uploads/f181aa8cc2b28060ade220dff4a973ea/3-7_file_tes.txt.jpg)

15. Melakukan transfer file untuk sample file menggunakan perintah `TRANSFER` di terminal sender.

![3-7_TRANSFER_sukses_sender](/uploads/80c20d06843e19ef169b4117fa0fe067/3-7_TRANSFER_sukses_sender.jpg)

16. Output pada terminal receiver ketika transfer file success.

![3-7_TRANSFER_sukses_receiver](/uploads/e468a57fe47d3addbaef50ab5378c3e0/3-7_TRANSFER_sukses_receiver.jpg)

17. isi folder Sender.

![3-7_isi_folder_Sender_](/uploads/135d14623b69854e9a3ca9827834f5b7/3-7_isi_folder_Sender_.jpg)

18. Isi folder Receiver yang menerima transfer file dari folder Sender.

![3-7_isi_folder_Receiver](/uploads/3c90c5c13a2f084b60e2c6b77c6aa24b/3-7_isi_folder_Receiver.jpg)

19. Kondisi ketika perintah `TRANSFER` diinput sebelum authentication success.

![3-7_TRANSFER_harus_AUTH](/uploads/15bb7918d6e87dfce1b941dc0595395a/3-7_TRANSFER_harus_AUTH.jpg)

20. Output pada terminal sender ketika user menginput command yang tidak dikenali.

![3-8_UNKNOWN_COMMAND](/uploads/9d11e9501ed6f67884b8b3915a7b17aa/3-8_UNKNOWN_COMMAND.jpg)

**C. REVISI**
Tidak ada.
### **IV. SOAL 4<a name="soal4"></a>**
**A. PEMBAHASAN**

Takumi adalah seorang pekerja magang di perusahaan Evil Corp. Dia mendapatkan tugas untuk membuat sebuah public room chat menggunakan konsep socket. Ketentuan lebih lengkapnya adalah sebagai berikut:

**POIN A.** Client dan server terhubung melalui socket. 

1. Penjelasan pada bagian client.c
```c
int main() {
    int socket_cl; //variabel untuk socket client
    struct sockaddr_in server_addr; //informasi alamat server
    char buffer[1024]; //buffer untuk pesan input

socket_cl = accept(server_socket, (struct sockaddr *)&client_addr, &client_addr_len);
if (socket_cl == -1) {
    perror("Error");
    exit(1);
}

printf("Client %d connected\n", client_id);
```

Program diawali dengan fungsi `main()`, yang merupakan fungsi utama yang akan dieksekusi ketika program dijalankan. Variabel `socket_cl` akan digunakan untuk menampung deskripsi socket client, `server_addr` akan digunakan untuk menyimpan informasi alamat server, dan `buffer` adalah buffer yang akan digunakan untuk menyimpan pesan yang akan dikirim ke server. Kemudian kode menciptakan socket client dengan menggunakan `socket()`. Ini menghasilkan deskripsi socket dan menyimpannya dalam variabel `socket_cl`. Jika pembuatan socket gagal, maka pesan kesalahan akan dicetak dengan `perror()` dan program akan keluar dengan kode kesalahan 1.

```c
    // Konfigurasi alamat server
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(1111); //menghubungkan ke port server
    server_addr.sin_addr.s_addr = inet_addr("127.0.0.1"); 
```
Kode di atas mengkonfigurasi alamat server yang akan dihubungi oleh client. `server_addr.sin_family` diatur sebagai AF_INET untuk menunjukkan penggunaan protokol IPv4. `server_addr.sin_port` diatur dengan port yang akan digunakan untuk koneksi ke server, dan `server_addr.sin_addr.s_addr` diatur dengan alamat IP server, dalam kasus ini "127.0.0.1" adalah alamat loopback, yang menunjukkan bahwa client akan berkomunikasi dengan server yang berjalan di mesin yang sama.

```c
    // Mencoba terhubung ke server
    if (connect(socket_cl, (struct sockaddr *)&server_addr, sizeof(server_addr)) == -1) {
        perror("Error in connecting to server"); //gagal terhubung ke server
        exit(1);
    }
```
Kode di atas mencoba untuk terhubung ke server dengan menggunakan  `connect()`. Jika koneksi gagal, maka pesan kesalahan akan dicetak dan program akan keluar.

2. Penjelasan pada bagian server.c
```c
socket_cl = accept(server_socket, (struct sockaddr *)&client_addr, &client_addr_len);
if (socket_cl == -1) {
    perror("Error");
    exit(1);
}

printf("Client %d connected\n", client_id);
```

**POIN B.** Server berfungsi sebagai penerima pesan dari client dan hanya menampilkan pesan saja.  
1. Penjelasan pada bagian client.c
```c
    while (1) {
        printf("Enter a message to send to the server (or 'exit' to quit): ");
        fgets(buffer, sizeof(buffer), stdin);

        if (strncmp(buffer, "exit", 4) == 0) {
            break; //keluar dari loop saat ada input 'exit'
        }
```
Program memasuki loop tak terbatas, di mana pengguna diminta untuk memasukkan pesan yang akan dikirimkan ke server. Jika pengguna memasukkan "exit", maka program akan keluar dari loop.
```c
        // Mengirim pesan ke server
        if (send(socket_cl, buffer, strlen(buffer), 0) == -1) {
            perror("Error in sending"); 
            exit(1);
        }
    }
```
Dalam loop, pesan yang dimasukkan oleh pengguna akan dikirim ke server menggunakan `send()`. Jika pengiriman pesan gagal, pesan kesalahan akan dicetak dan program akan keluar.
```c
    close(socket_cl); //menutup socket saat selesai
    return 0;
}
```
Terakhir, program menutup socket client dengan close(), dan kemudian mengembalikan nilai 0 sebagai indikasi bahwa program selesai dengan sukses.

2. Penjelasan pada bagian server.c
```c
typedef struct {
    int socket_cl;
    int client_id;
} ClientInfo;

```
Ini adalah definisi struktur `ClientInfo` yang digunakan untuk menyimpan informasi tentang setiap klien yang terhubung ke server. Struktur ini memiliki dua anggota, yaitu `socket_cl` yang merupakan socket untuk komunikasi dengan klien dan `client_id` yang merupakan ID unik untuk mengidentifikasi setiap klien.
```c
void *handle_client(void *client_info_ptr) { //menangani komunikasi client-server
    ClientInfo *client_info = (ClientInfo *)client_info_ptr;
    int socket_cl = client_info->socket_cl;
    int client_id = client_info->client_id;
    char buffer[1024];

    while (1) {
        memset(buffer, 0, sizeof(buffer));
        int bytes_received = recv(socket_cl, buffer, sizeof(buffer), 0);
        if (bytes_received == -1) {
            perror("Error in receiving");
            break;
        } else if (bytes_received == 0) {
            printf("Client %d disconnected\n", client_id);
            printf("========================\n");
            break;
        } else {
            printf("Message from client %d: %s", client_id, buffer);
        }
    }

    close(socket_cl);
    free(client_info);
    return NULL;
}
```
Ini adalah fungsi yang akan dijalankan dalam thread terpisah untuk menangani komunikasi dengan setiap klien. Fungsi ini menerima parameter berupa pointer ke struktur `ClientInfo`. Fungsi ini akan menerima pesan dari klien dan mencetaknya di layar. Jika ada kesalahan dalam menerima data atau klien memutuskan koneksi, loop akan berakhir. Setelah itu, socket klien akan ditutup, dan memori yang dialokasikan untuk `client_info` akan dibebaskan.
```c
int main() {
    int server_socket, socket_cl;
    struct sockaddr_in server_addr, client_addr;
    socklen_t client_addr_len = sizeof(client_addr);

    server_socket = socket(AF_INET, SOCK_STREAM, 0); //inisialisasi socket server
    if (server_socket == -1) {
        perror("Error in socket creation");
        exit(1);
    }

    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(1111);
    server_addr.sin_addr.s_addr = INADDR_ANY;

    //bind ke addr
    if (bind(server_socket, (struct sockaddr *)&server_addr, sizeof(server_addr)) == -1) {
        perror("Error in binding");
        exit(1);
    }

    if (listen(server_socket, MAX_CLIENTS) == -1) {  //listen saat koneksi masuk
        perror("Error in listening");
        exit(1);
    }

    printf("Server listening on port 1111...\n");
```

Dalam blok kode ini, fungsi main pertama-tama menginisialisasi server socket dengan menggunakan `socket()` untuk membuat socket, kemudian mengatur alamat dan port yang akan digunakan oleh server dengan mengisi struktur `server_addr`. Selanjutnya, program melakukan bind ke alamat dan port tersebut menggunakan `bind()`, dan kemudian memulai proses mendengarkan koneksi masuk menggunakan `listen()`. Jika ada kesalahan dalam proses inisialisasi soket atau mengikat alamat, program akan mencetak pesan kesalahan dan keluar. Setelah semuanya siap, program mencetak pesan bahwa server sedang mendengarkan di port 1111. Server sekarang siap menerima koneksi dari klien.

**POIN C.** karena resource Evil Corp sedang terbatas buatlah agar server hanya bisa membuat koneksi dengan 5 client saja. 

Penjelasan pada bagian server.c
```c
#define MAX_CLIENTS 5
```
Disini didefinisikan konstanta `MAX_CLIENTS` yang digunakan untuk menentukan jumlah maksimum klien yang dapat terhubung ke server.
```c
int client_id = 1;
    int max = 0;
    char *max_reached_message = NULL;

    while (1) {
        if (client_id > MAX_CLIENTS) {
            if (max) {
                if (max_reached_message) {
                    printf("%s\n", max_reached_message);
                    free(max_reached_message);
                    max_reached_message = NULL;
                }
                break;
            } else {
                max_reached_message = "Client total has reached the maximum number.";
                max = 1;
            }
        }

        socket_cl = accept(server_socket, (struct sockaddr *)&client_addr, &client_addr_len);
        if (socket_cl == -1) {
            perror("Error");
            exit(1);
        }

        printf("Client %d connected\n", client_id);

        //memasukkan informasi client 
        ClientInfo *client_info = (ClientInfo *)malloc(sizeof(ClientInfo));
        client_info->socket_cl = socket_cl;
        client_info->client_id = client_id;

        //thread untuk komunikasi ke client
        pthread_t thread;
        if (pthread_create(&thread, NULL, handle_client, client_info) != 0) {
            perror("Error in creating thread");
            exit(1);
        }

        //pesan saat total client sudah max
        if (client_id > MAX_CLIENTS) {
            max_reached_message = "Client total has reached the maximum number.";
        }
        client_id++;
    }
    close(server_socket);

    return 0;
}
```
Dalam blok kode di atas, terdapat sebuah loop yang terus berjalan (while loop) untuk menerima koneksi dari client. Jika jumlah koneksi client `(client_id)` melebihi batas yang telah ditetapkan `(MAX_CLIENTS)`, maka program akan mencapai batas maksimum dan keluar dari loop. Saat server menerima koneksi baru dari seorang client, ia mencetak pesan "Client [nomor client] connected" ke layar, kemudian mengalokasikan memori untuk menyimpan informasi client, seperti socket client dan ID client. Selanjutnya, program menciptakan sebuah thread untuk setiap client dengan menggunakan `pthread_create()`, sehingga setiap client memiliki thread sendiri untuk menangani komunikasi dengan server. Jika jumlah client mencapai batas maksimum, program juga mengatur pesan "Client total has reached the maximum number" yang akan ditampilkan di server. Setelah menerima dan menangani koneksi semua client, server menutup socket server dan program selesai.

**B. OUTPUT** 
1. Isi folder soal 4 setelah server.c dan client.c dicompile menggunakan `gcc server.c -o server -lpthread` dan `gcc client.c -o client -lpthread`.

![4-1](/uploads/69bebe5f3022afb363fc5e0b9f2632dc/4-1.jpg)

2. Server dijalankan di terminal menggunakan `./server`

![4-2_server_listeing](/uploads/24a77169848e08af55124b8695a7f361/4-2_server_listeing.jpg)

3. Menjalankan 6 client pada terminal yang berbeda menggunakan `./client`

![4-3_c1](/uploads/890932723f647bf9f77c9b744aea50ed/4-3_c1.jpg)

![4-3_c2](/uploads/36cbd121cec5a186339120f941e6118f/4-3_c2.jpg)

![4-3_c3](/uploads/38b059ad5975147caf51440afefe87af/4-3_c3.jpg)

![4-3_c4](/uploads/a2c979466e9654fe8ac4d63a3cd273d4/4-3_c4.jpg)

![4-3_c5](/uploads/12f29a92ede0b2f863118e990f2c09bb/4-3_c5.jpg)

![4-3_c6_gagal](/uploads/71863634bf5b1f257998f3009e913bec/4-3_c6_gagal.jpg)

4. Output yang diterima server saat 6 client dijalankan.

![4-4_server_full](/uploads/c521e035b8fa76f534f14062f73f22ac/4-4_server_full.jpg)

Disini client 6 tidak dapat connect ke server karena sudah mencapai batas maksimum (5 client) dan pesan dari client ke 6 tidak diterima server.

5. Mencoba jika salah satu client diexit dan client ke 6 dijalankan kembali.

![4-5_c3_disconnected](/uploads/708268b551d5bfcbdb10629d6259f84e/4-5_c3_disconnected.jpg)

![4-7_c6_connect](/uploads/754f8040adc3016b2f3940b27244e58a/4-7_c6_connect.jpg)

6. Output yang diterima server saat client ke 6 dijalankan kembali.

![4-8_server_c6](/uploads/b6ad5ff4f99d7819ad35c16d80a61058/4-8_server_c6.jpg)

Disini client 6 dapat connect ke server dan pesan dari client 6 dapat diterima server karena client ke 3 sudah disconnected, sehingga client yang connect berjumlah 5 (belum melewati batas maksimum).

**C. REVISI**  
Pada kode sebelumnya, server dibatasi untuk 5 client. Namun, ketika salah satu client disconnected, server tetap tidak dapat menerima koneksi client baru. Sedangkan, pada soal diminta bahwa server akan berhenti menerima koneksi jika 5 client connect secara bersamaan, sehingga ketika salah satu koneksinya terputus, server dapat menerima koneksi 1 client baru lagi.
Berikut adalah perbaikan pada kode:
```c
pthread_t client_threads[MAX_CLIENTS];
```
Menambahkan array `pthread_t client_threads[MAX_CLIENTS]` untuk menyimpan thread yang digunakan untuk menangani setiap klien. Ini memungkinkan penanganan simultan klien dalam thread terpisah.
```c
int client_id = 1;

while (1) {
    socket_cl = accept(server_socket, (struct sockaddr *)&client_addr, &client_addr_len);
    if (socket_cl == -1) {
        perror("Error");
        exit(1);
    }

    if (connected_clients < MAX_CLIENTS) {
        printf("Client %d connected\n", client_id);

        // Menambahkan 1 ke connected_clients saat klien terhubung
        connected_clients++;

        // Buat struktur ClientInfo untuk menyimpan informasi klien
        ClientInfo *client_info = (ClientInfo *)malloc(sizeof(ClientInfo));
        client_info->socket_cl = socket_cl;
        client_info->client_id = client_id;

        // Buat thread untuk menangani klien
        if (pthread_create(&client_threads[client_id - 1], NULL, handle_client, client_info) != 0) {
            perror("Error creating thread");
            exit(1);
        }

        // ...

        client_id++;
    } else {
        // Jika sudah mencapai batas maksimum, tolak koneksi tambahan atau atasi sesuai kebutuhan.
        printf("Client connection limit reached. Rejecting connection.\n");
        close(socket_cl);
    }
}
```
Disini dimodifikasi loop utama server untuk menangani setiap koneksi klien dalam thread terpisah. Program akan membuat thread baru untuk menangani klien dengan menggunakan `pthread_create`.
```c
void *handle_client(void *client_info_ptr) {
    ClientInfo *client_info = (ClientInfo *)client_info_ptr;
    int socket_cl = client_info->socket_cl;
    int client_id = client_info->client_id;
    char buffer[1024];

    // ...

    // Kurangi connected_clients saat klien terminasi
    connected_clients--;

    printf("Client %d disconnected\n", client_id);
    printf("========================\n");

    close(socket_cl);
    free(client_info);

    // Keluar dari thread
    pthread_exit(NULL);
    return NULL;
}
```
Dalam fungsi `handle_client`, hapus thread setelah klien selesai dengan menggunakan `pthread_exit`. Ini memungkinkan server untuk terus berjalan dan menerima klien baru ketika ada klien yang terputus.
Dengan perubahan ini, server akan terus menerima koneksi client hingga mencapai batas maksimum (5 client) yang terkoneksi secara bersamaan.
