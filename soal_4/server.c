#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <pthread.h>

#define MAX_CLIENTS 5

typedef struct {
    int socket_cl;
    int client_id;
} ClientInfo;

int connected_clients = 0;
pthread_t client_threads[MAX_CLIENTS];

void *handle_client(void *client_info_ptr) {
    ClientInfo *client_info = (ClientInfo *)client_info_ptr;
    int socket_cl = client_info->socket_cl;
    int client_id = client_info->client_id;
    char buffer[1024];

    while (1) {
        memset(buffer, 0, sizeof(buffer));
        int bytes_received = recv(socket_cl, buffer, sizeof(buffer), 0);
        if (bytes_received == -1) {
            perror("Error in receiving");
            break;
        } else if (bytes_received == 0) {
            printf("Client %d disconnected\n", client_id);
            printf("========================\n");
            break;
        } else {
            printf("Message from client %d: %s", client_id, buffer);
        }
    }

    connected_clients--;

    close(socket_cl);
    free(client_info);

    pthread_exit(NULL);
    return NULL;
}

int main() {
    int server_socket, socket_cl;
    struct sockaddr_in server_addr, client_addr;
    socklen_t client_addr_len = sizeof(client_addr);

    server_socket = socket(AF_INET, SOCK_STREAM, 0);
    if (server_socket == -1) {
        perror("Error in socket creation");
        exit(1);
    }

    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(1234);
    server_addr.sin_addr.s_addr = INADDR_ANY;

    if (bind(server_socket, (struct sockaddr *)&server_addr, sizeof(server_addr)) == -1) {
        perror("Error in binding");
        exit(1);
    }

    if (listen(server_socket, MAX_CLIENTS) == -1) {
        perror("Error in listening");
        exit(1);
    }

    printf("Server listening on port 1111...\n");

    int client_id = 1;

    while (1) {
        socket_cl = accept(server_socket, (struct sockaddr *)&client_addr, &client_addr_len);
        if (socket_cl == -1) {
            perror("Error");
            exit(1);
        }

        if (connected_clients < MAX_CLIENTS) {
            printf("Client %d connected\n", client_id);
            connected_clients++;

            ClientInfo *client_info = (ClientInfo *)malloc(sizeof(ClientInfo));
            client_info->socket_cl = socket_cl;
            client_info->client_id = client_id;

            if (pthread_create(&client_threads[client_id - 1], NULL, handle_client, client_info) != 0) {
                perror("Error creating thread");
                exit(1);
            }

            client_id++;
        } else {
            printf("Client connection limit reached. Rejecting connection.\n");
            close(socket_cl);
        }
    }

    close(server_socket);

    return 0;
}
