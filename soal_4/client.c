#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>

int main() {
    int socket_cl; //variabel untuk socket client
    struct sockaddr_in server_addr; //informasi alamat server
    char buffer[1024]; //buffer untuk pesan input

    socket_cl = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_cl == -1) {
        perror("Error in socket creation"); //gagal membuat socket
        exit(1);
    }

    // Konfigurasi alamat server
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(1111); //menghubungkan ke port server
    server_addr.sin_addr.s_addr = inet_addr("127.0.0.1"); 
    //mencoba terhubung ke server
    if (connect(socket_cl, (struct sockaddr *)&server_addr, sizeof(server_addr)) == -1) {
        perror("Error in connecting to server"); //gagal terhubung ke server
        exit(1);
    }

    while (1) {
        printf("Enter a message to send to the server (or 'exit' to quit): ");
        fgets(buffer, sizeof(buffer), stdin);

        if (strncmp(buffer, "exit", 4) == 0) {
            break; //keluar dari loop saat ada input 'exit'
        }

        // Mengirim pesan ke server
        if (send(socket_cl, buffer, strlen(buffer), 0) == -1) {
            perror("Error in sending"); 
            exit(1);
        }
    }

    close(socket_cl); //menutup socket saat selesai
    return 0;
}

